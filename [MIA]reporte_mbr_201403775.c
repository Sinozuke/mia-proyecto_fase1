#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_mbr(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    char comando_dot[100] = "dot -tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, path);
    strcat(comando_dot, ".jpg MBR.dot");

    strcat(comando_abertura, path);
    strcat(comando_abertura, ".jpg");

    FILE *disco;

    disco = fopen(path_disco, "r+b");

    MBR mbr;
    EBR ebr;

    fseek(disco, 0, SEEK_SET);
    fread(&mbr, sizeof (mbr), 1, disco);

    FILE *reporte;

    reporte = fopen("MBR.dot", "w");
    int extendida = 0;

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "\t\"mbr\" [label=<\n");
    fprintf(reporte, "<TABLE>\n");
    fprintf(reporte, "\t<TR>\n");
    fprintf(reporte, "\t\t<TD COLSPAN=\"2\">MBR %s.dsk</TD>\n", path_disco);
    fprintf(reporte, "</TR>\n");
    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>Nombre</TD>\n");
    fprintf(reporte, "<TD>Valor</TD>\n");
    fprintf(reporte, "</TR>\n");
    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>mbr_tamanio</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_tamano);
    fprintf(reporte, "</TR>\n");
    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>mbr_fecha_creacion</TD>\n");
    fprintf(reporte, "<TD>%s</TD>\n", ctime(&mbr.mbr_fecha_creacion));
    fprintf(reporte, "</TR>\n");
    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>mbr_disk_signature</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_disk_signature);
    fprintf(reporte, "</TR>\n");

    /* escritura de cada una de las particoines */

    for (int i = 0; i < 4; i++) {
        switch (i) {
            case 0:
                if (mbr.mbr_partition_1.part_status != 'n') {
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_status_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_1.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_type_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_1.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_fit_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_1.part_fit);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_start_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_1.part_start);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_size_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_1.part_size);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_name_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%s</TD>\n", mbr.mbr_partition_1.part_name);
                    fprintf(reporte, "</TR>\n");

                    if (mbr.mbr_partition_1.part_type == 'e')
                        extendida = 1;
                }
                break;
            case 1:
                if (mbr.mbr_partition_2.part_status != 'n') {
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_status_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_2.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_type_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_2.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_fit_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_2.part_fit);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_start_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_2.part_start);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_size_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_2.part_size);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_name_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%s</TD>\n", mbr.mbr_partition_2.part_name);
                    fprintf(reporte, "</TR>\n");
                    if (mbr.mbr_partition_2.part_type == 'e')
                        extendida = 2;
                }
                break;
            case 2:
                if (mbr.mbr_partition_3.part_status != 'n') {
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_status_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_3.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_type_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_3.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_fit_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_3.part_fit);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_start_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_3.part_start);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_size_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_3.part_size);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_name_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%s</TD>\n", mbr.mbr_partition_3.part_name);
                    fprintf(reporte, "</TR>\n");
                    if (mbr.mbr_partition_3.part_type == 'e')
                        extendida = 3;
                }
                break;
            case 3:
                if (mbr.mbr_partition_4.part_status != 'n') {
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_status_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_4.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_type_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_4.part_status);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_fit_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%c</TD>\n", mbr.mbr_partition_4.part_fit);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_start_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_4.part_start);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_size_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%i</TD>\n", mbr.mbr_partition_4.part_size);
                    fprintf(reporte, "</TR>\n");
                    fprintf(reporte, "<TR>\n");
                    fprintf(reporte, "<TD>part_name_%i</TD>\n", i + 1);
                    fprintf(reporte, "<TD>%s</TD>\n", mbr.mbr_partition_4.part_name);
                    fprintf(reporte, "</TR>\n");
                    if (mbr.mbr_partition_4.part_type == 'e')
                        extendida = 4;
                }
                break;
        }
    }

    fprintf(reporte, "</TABLE>>];");

    if (extendida != 0) {

        switch (extendida) {
            case 1:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 2:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 3:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 4:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
        }

        fread(&ebr, sizeof (ebr), 1, disco);
        int contador = 1;
        while (ebr.part_next != -1) {
            if (ebr.part_status != 'n') {
                fprintf(reporte, "\"ebr%i\" [label=<\n", contador);
                fprintf(reporte, "<TABLE>\n");
                fprintf(reporte, "\t<TR>\n");
                fprintf(reporte, "\t\t<TD COLSPAN=\"2\">EBR_%i</TD>\n", contador);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>Nombre</TD>\n");
                fprintf(reporte, "<TD>Valor</TD>\n");
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_satus_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%c</TD>\n", ebr.part_status);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_fit_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%c</TD>\n", ebr.part_fit);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_start_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%i</TD>\n", ebr.part_start);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_size_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%i</TD>\n", ebr.part_size);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_next_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%i</TD>\n", ebr.part_next);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>part_name_%i</TD>\n", contador);
                fprintf(reporte, "<TD>%s</TD>\n", ebr.part_name);
                fprintf(reporte, "</TR>\n");
                fprintf(reporte, "</TABLE>>];");
            }
            fseek(disco, ebr.part_next, SEEK_SET);
            fread(&ebr, sizeof (EBR), 1, disco);
            contador++;
        }
        contador++;
        if (ebr.part_status != 'n') {
            fprintf(reporte, "\"ebr%i\" [label=<\n", contador);
            fprintf(reporte, "<TABLE>\n");
            fprintf(reporte, "\t<TR>\n");
            fprintf(reporte, "\t\t<TD COLSPAN=\"2\">EBR_%i</TD>\n", contador);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>Nombre</TD>\n");
            fprintf(reporte, "<TD>Valor</TD>\n");
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_satus_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%c</TD>\n", ebr.part_status);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_fit_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%c</TD>\n", ebr.part_fit);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_start_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%i</TD>\n", ebr.part_start);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_size_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%i</TD>\n", ebr.part_size);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_next_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%i</TD>\n", ebr.part_next);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>part_name_%i</TD>\n", contador);
            fprintf(reporte, "<TD>%s</TD>\n", ebr.part_name);
            fprintf(reporte, "</TR>\n");
            fprintf(reporte, "</TABLE>>];");
        }

    }

    fprintf(reporte, "}");


    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");

}
