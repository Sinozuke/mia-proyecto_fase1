#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_block(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_dot[100] = "dot -Tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, directorio);
    strcat(comando_dot, ".jpg BLOCK.dot");

    strcat(comando_abertura, directorio);
    strcat(comando_abertura, ".jpg");


    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;

    reporte = fopen("BLOCK.dot", "w");

    SB sb;
    bloque_archivo archivo;
    bloque_carpeta carpeta;
    bloque_apuntadores apuntador;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "rankdir=LR;\n");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    char bloque = 'v';

    for (int i = 0; i < sb.s_blocks_count; i++) {
        fseek(disco, sb.s_bm_block_start +i*sizeof(char), SEEK_SET);
        fread(&bloque, sizeof (char), 1, disco);
        if (bloque == 'd') {
            fprintf(reporte,"\"%i\" [label=<\n",i);
            fprintf(reporte,"<TABLE>\n");
            fprintf(reporte,"<TR>\n");
            fprintf(reporte,"<TD COLSPAN=\"2\">Bloque Carpeta %i</TD>\n",i+1);
            fprintf(reporte,"</TR>\n");
            fprintf(reporte,"<TR>\n");
            fprintf(reporte,"<TD>b_name</TD>\n");
            fprintf(reporte,"<TD>b_inodo</TD>\n");
            fprintf(reporte,"</TR>\n");

            fseek(disco,sb.s_block_start+i*sizeof(bloque_carpeta),SEEK_SET);
            fread(&carpeta,sizeof(bloque_carpeta),1,disco);

            for(int j=0;j<4;j++){
                fprintf(reporte,"<TR>\n");
                if(carpeta.b_content[j].b_inodo==-1){
                    fprintf(reporte,"<TD>-</TD>\n");
                }else{
                    fprintf(reporte,"<TD>%s</TD>\n",carpeta.b_content[j].bname);
                }
                fprintf(reporte,"<TD>%i</TD>\n",carpeta.b_content[j].b_inodo);
                fprintf(reporte,"</TR>\n");
            }

            fprintf(reporte,"</TABLE>>];\n");
        }else if(bloque == 'f'){
            int contador=0;
            fseek(disco,sb.s_block_start+i*sizeof(bloque_archivo),SEEK_SET);
            fread(&archivo,sizeof(bloque_archivo),1,disco);
            fprintf(reporte,"\"%i\" [label=<\n",i);
            fprintf(reporte,"<TABLE>\n");
            fprintf(reporte,"<TR>\n");
            fprintf(reporte,"<TD>Bloque Archivo %i</TD>\n",i+1);
            fprintf(reporte,"</TR>\n");
            fprintf(reporte,"<TR>\n");
            fprintf(reporte,"<TD>");
            for (int j=0;j<64;j++){
                if(archivo.b_content[j]=='\n'){
                    fprintf(reporte,"\\n");
                contador++;
                }else if(archivo.b_content[j]!='\0'){
                    fprintf(reporte,"%c",archivo.b_content[j]);                
                contador++;
                }
                if(contador==15){
                    fprintf(reporte,"<BR/>");
                    contador=0;
                }
            }
            fprintf(reporte,"</TD>\n");
            fprintf(reporte,"</TR>\n");
            fprintf(reporte,"</TABLE>>];\n");
        }else if(bloque == 'a'){
            int contador=0;
            fseek(disco,sb.s_block_start+i*sizeof(bloque_apuntadores),SEEK_SET);
            fread(&apuntador,sizeof(bloque_apuntadores),1,disco);
            fprintf(reporte,"%i\" [label=<\n",i);
            fprintf(reporte,"<TABLE>\n");
            fprintf(reporte,"<TR>\n");
            fprintf(reporte,"<TD>Bloque Apuntadores %i</TD>\n",i+1);
            fprintf(reporte,"</TR>\n");
            fprintf(reporte,"<TR>");
            fprintf(reporte,"<TD>");
            for(int j=0;j<16;j++){
                fprintf(reporte,"%i,",apuntador.b_pointers[j]);
                contador++;
                if(contador==6){
                    fprintf(reporte,"<BR/>");
                    contador=0;
                }
            }
            fprintf(reporte,"</TD>");            
            fprintf(reporte,"</TR>");
            
            fprintf(reporte,"</TABLE>>];\n");
        }
    }

    int anterior=0;
    int actual=0;
    fseek(disco, sb.s_bm_block_start, SEEK_SET);

    for (int i = 0; i < sb.s_blocks_count; i++) {
        fread(&bloque, sizeof (char), 1, disco);
        if(bloque!='0'){
            anterior = actual;
            actual=i;
            if(actual!=0)
                fprintf(reporte,"\"%i\"->\"%i\";\n",anterior,actual);
        }
    }

    fprintf(reporte, "}");
    
    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}
