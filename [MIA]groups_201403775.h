#include "[MIA]Datos_201403775.h"
#ifndef GROUPS_H
#define GROUPS_H

void crear_grupo(Montaje *montaje, Sesion *sesion, char *nombre, char *id);
void eliminar_grupo(Montaje *montaje, Sesion *sesion, char *nombre, char *id);

#endif /* GROUPS_H */

