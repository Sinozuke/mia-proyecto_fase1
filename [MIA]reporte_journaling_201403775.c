#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_journaling(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_abertura[100] = "gnome-open ";

    strcat(comando_abertura, directorio);


    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;


    SB sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    if (sb.s_file_system_type != 3) {
        printf("ERROR: La Particion que se ha montado no tiene un sitema de archivos Ext3.\n");
        printf("-- REPORTE FALLIDO --\n");
        return;
    }

    reporte = fopen(directorio, "w");

    Journal jour;

    for (int i = 0; i < sb.s_inodes_count; i++) {
        fread(&jour, sizeof (Journal), 1, disco);
        if (jour.Journal_tipo != '-') {
            if (jour.Journal_tipo_operacion == 'c') {
                fprintf(reporte, "Tipo de operacion: Creacion\n");
            } else {
                fprintf(reporte, "Tipo de operacion: Modificacion\n");
            }
            if (jour.Journal_tipo == 'f') {
                fprintf(reporte, "Tipo: \"Archivo\"\n");
            } else {
                fprintf(reporte, "Tipo: \"Directorio\"\n");
            }
            fprintf(reporte, "Nombre: \"%s\"\n", jour.Journal_nombre);
            fprintf(reporte, "Contenido: \"%s\"\n", jour.Journal_contenido);
            fprintf(reporte, "Fecha de Tancision: %s\n", ctime(&jour.Journal_fecha));
            fprintf(reporte, "Propetario: \"%s\"\n", jour.Journal_nombre);
            fprintf(reporte, "Permisos: %i\n\n\n", jour.Journal_Permisos);
        }
    }

    fclose(reporte);
    fclose(disco);

    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}
