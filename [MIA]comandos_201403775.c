#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]comandos_201403775.h"
#include "[MIA]Aux_comandos_201403775.h"
#include "[MIA]funciones_201403775.h"

void subMain(char *comando, Montaje *montaje, Sesion *sesion);

void mkdisk(char *token) {
    char valor[500];
    char *valor_r;
    char *path;
    char *nombre;
    int valor_size;
    int p_size = 0, p_unit = 0, p_path = 0, p_name = 0;
    int kilo_mega = 1;
    int i;
    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 1:
                if (!p_size) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'size' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (!isnumero(valor_r)) {
                        printf("ERROR el Valor ingresado para size no es un numero.\n");
                        return;
                    }
                    sscanf(valor_r, "%d", &valor_size);
                    if (valor_size <= 0) {
                        printf("ERROR: el valor \"%s\" para el aprametro size no es aceptado.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: Parametro 'size' declarado mas de una vez.\n");
                    return;
                }
                p_size = 1;
                break;
            case 2:
                if (!p_unit) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'unit' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    kilo_mega = k_m(valor_r);
                    if (kilo_mega == 0 || kilo_mega == 3) {
                        printf("el valor \"%s\" para el parametro unit no es aceptado.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: Parametro 'unit' declarado mas de una vez.\n");
                    return;
                }
                p_unit = 1;
                break;
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    path = malloc(sizeof (char)*strlen(valor_r));
                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    nombre = malloc(sizeof (char)*strlen(valor_r));
                    strcpy(nombre, valor_r);
                } else {
                    printf("ERROR: Parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                p_name = 1;
                break;
            default:
                printf("Error: el parametro \"%s\" no es reconocido\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_size * p_path * p_name) {
        printf("-------------CREANDO DISCO-------------\n");
        printf("-----------------DATOS-----------------\n");
        if (kilo_mega == 1)
            printf("Tamaño:\t%i\nMedida:\t%s\nPath:\t%s\nNombre:\t%s\n", valor_size, "Mb", path, nombre);
        else
            printf("Tamaño:\t%i\nMedida:\t%s\nPath:\t%s\nNombre:\t%s\n", valor_size, "Kb", path, nombre);
        printf("-----------------GENERANDO-----------------\n");
        if (kilo_mega == 1)
            generar_disco(nombre, path, valor_size, 1);
        else
            generar_disco(nombre, path, valor_size, 0);
    } else {
        if (!p_size)
            printf("el parametro 'size' no se ha encontrado\n");
        if (!p_name)
            printf("el parametro 'name' no se ha encontrado\n");
        if (!p_path)
            printf("el parametro 'path' no se ha encontrado\n");
        return;
    }
    free(valor_r);
    free(path);
    free(nombre);
}

void rmdisk(Montaje *montajes, char *token) {
    char valor[500];
    char *valor_r;
    char *path;
    int p_path = 0;
    token = strtok(NULL, " ");
    int i;
    while (token) {
        if (parametros(token) == 3) {
            if (!p_path) {
                if (!pasa(token, 1, 0)) {
                    printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                    return;
                }
                if (!pasa(token, 0, 1)) {
                    printf("ERROR: el valor del parametro 'path' no esta encerrado entre comillas.\n");
                    return;
                }
                for (i = 0; token[i + 7] != '\0'; i++)
                    valor[i] = token[i + 7];
                valor[i] = '\0';
                valor_r = valor_real(longitud_real(valor), valor);
                valor_r = ncomillas(valor_r);
                path = malloc(sizeof (char)*strlen(valor_r));
                strcpy(path, valor_r);
            } else {
                printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                return;
            }
            p_path = 1;
        } else {
            printf("Error: el parametro \"%s\" no es reconocido\n", token);
            return;
        }
        token = strtok(NULL, " ");
    }
    if (p_path) {
        printf("---------------BORRANDO DISCO---------------\n");
        eliminar_disco(montajes, path);
    } else {
        printf("el parametro 'path' no se ha encontrado\n");
        return;
    }
}

void fdisk(char *token) {
    int p_size = 0;
    int p_unit = 0;
    int p_path = 0;
    int p_name = 0;
    int p_type = 0;
    int p_fit = 0;
    int p_delete = 0;
    int p_add = 0;

    int add = 0;
    int size;
    int unit = 2;
    char path[200];
    char *name;
    char type = 'p';
    char fit = 'w';
    char vdelete;

    char valor[100];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 1:
                if (!p_size) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'size' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (!isnumero(valor_r)) {
                        printf("ERROR el Valor ingresado para size no es un numero.\n");
                        return;
                    }
                    sscanf(valor_r, "%d", &size);
                    if (size <= 0) {
                        printf("ERROR: el valor \"%s\" para el aprametro size no es aceptado.\n", valor_r);
                        return;
                    }
                    p_size = 1;
                } else {
                    printf("ERROR: el parametro 'size' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 2:
                if (!p_unit) {
                    if (!p_unit) {
                        if (!pasa(token, 1, 0)) {
                            printf("ERROR: no se presenta el separador '::' entre el parametro 'unit' y el valor.\n");
                            return;
                        }
                        for (i = 0; token[i + 7] != '\0'; i++)
                            valor[i] = token[i + 7];
                        valor[i] = '\0';
                        valor_r = valor_real(longitud_real(valor), valor);
                        unit = k_m(valor_r);
                        if (unit == 0) {
                            printf("el valor \"%s\" para el parametro unit no es aceptado.\n", valor_r);
                            return;
                        }
                    } else {
                        printf("ERROR: Parametro 'unit' declarado mas de una vez.\n");
                        return;
                    }
                    p_unit = 1;
                } else {
                    printf("ERROR: el parametro 'unit' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: el parametro 'path' esta declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    name = malloc(sizeof (char)*strlen(valor_r));
                    strcpy(name, valor_r);
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                p_name = 1;
                break;
            case 5:
                if (!p_type) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'type' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (strlen(valor_r) != 1) {
                        printf("ERROR: valor \"%s\" no permitido para el parametro 'type'.\n", valor_r);
                        return;
                    }
                    type = valor_r[0];
                    if (!(type == 'P' || type == 'E' || type == 'L' || type == 'p' || type == 'e' || type == 'l')) {
                        printf("ERROR: valor \"%s\" no permitido para el parametro 'type'.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'type' esta declarado mas de una vez.\n");
                    return;
                }
                p_type = 1;
                break;
            case 6:
                if (!p_fit) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'type' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (strlen(valor_r) != 2) {
                        printf("ERROR: valor \"%s\" no permitido para el parametro 'fit'.\n", valor_r);
                        return;
                    }
                    if ((fit = BF_FF_WF(valor_r)) == 'n') {
                        printf("ERROR: valor \"%s\" no permitido para el parametro 'fit'.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'fit' esta declarado mas de una vez.\n");
                    return;
                }
                p_fit = 1;
                break;
            case 7:
                if (!p_delete) {
                    if (p_add) {
                        printf("ERROR: declaracion previa de parametro incopatible 'add'.\n");
                        return;
                    }
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'delete' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 9] != '\0'; i++)
                        valor[i] = token[i + 9];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    vdelete = Fast_Full(valor_r);

                    if (vdelete == 'n') {
                        printf("ERROr el valor \"%s\" para el parametro 'delete' no es valido.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'delete' esta declarado mas de una vez.\n");
                    return;
                }
                p_delete = 1;
                break;
            case 8:
                if (!p_add) {
                    if (p_delete) {
                        printf("ERROR: declaracion previa de parametro incopatible 'delete'.\n");
                        return;
                    }
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'delete' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (!isnumero(valor_r)) {
                        printf("ERROR el Valor ingresado para size no es un numero.\n");
                        return;
                    }
                    add = atoi(valor_r);
                    if (add == 0) {
                        printf("ERROr el valor \"%i\" para el parametro 'add' no es valido.\n", add);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'add' esta declarado mas de una vez.\n");
                    return;
                }
                p_add = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_delete) {
        if (p_name * p_path) {
            if (p_fit)
                printf("ADVERTENCIA: El parametro 'fit' sera ignorado dado que no es nesesario para su eliminacion.\n");
            if (p_type)
                printf("ADVERTENCIA: El parametro 'type' sera ignorado dado que no es nesesario para su eliminacion.\n");
            if (p_unit)
                printf("ADVERTENCIA: El parametro 'unit' sera ignorado dado que no es nesesario para su eliminacion.\n");
            if (p_size)
                printf("ADVERTENCIA: El parametro 'size' sera ignorado dado que no es nesesario para su eliminacion.\n");
            eliminar_particion(name, path, vdelete);
        } else {
            if (!p_name)
                printf("ERROR: para eliminar una particion es nesario el nombre de la misma.\n");
            if (!p_path)
                printf("ERROR: para eliminar una particion es nesario el path en donde se encuentra el disco.\n");
            return;
        }
    } else if (p_add) {
        if (p_unit * p_name * p_path) {
            if (p_size)
                printf("ADVERTENCIA: El parametro 'size' sera ignorado dado que no es nesesario para su eliminacion.\n");
            if (p_type)
                printf("ADVERTENCIA: El parametro 'type' sera ignorado dado que no es nesesario para su eliminacion.\n");
            if (p_fit)
                printf("ADVERTENCIA: El parametro 'fit' sera ignorado dado que no es nesesario para su eliminacion.\n");
            switch (unit) {
                case 1:
                    modificar_tamano_particion(name, path, 'm', add);
                    break;
                case 2:
                    modificar_tamano_particion(name, path, 'k', add);
                    break;
                case 3:
                    modificar_tamano_particion(name, path, 'b', add);
                    break;
            }
        } else {
            if (!p_name)
                printf("ERROR: para modificar el tamano en una particion es nesario el nombre de la misma.\n");
            if (!p_path)
                printf("ERROR: para modificar el tamano en una particion es nesario el path del disco en donde esta la misma.\n");
            if (!p_unit)
                printf("ERROR: para modificar el tamano en una particion es nesario especificar en que medida se aumentara/disminuira la misma.\n");
            return;
        }
    } else {
        if (p_name * p_path * p_size) {
            if (((int) strlen(name)) > 15) {
                printf("ERROR: el nombre para la particion exede el tamano permitido de 14 caracteres.\n");
                return;
            }
            switch (unit) {
                case 1:
                    if (size < 10) {
                        printf("ERROR: el tamano especificado es menor al limite permitido (10 MB).\n");
                        return;
                    }
                    break;
                case 2:
                    if (size / 1024 < 10) {
                        printf("ERROR: el tamano especificado es menor al limite permitido (10,240 KB).\n");
                        return;
                    }
                    break;
                case 3:
                    if (((size) / 1024) / 1024 < 10) {
                        printf("ERROR: el tamano especificado es menor al limite permitido (10,485,760 B).\n");
                        return;
                    }
                    break;
            }
            if (type == 'p' || type == 'P')
                crear_pp(name, path, size, fit, unit);
            if (type == 'e' || type == 'E')
                crear_pe(name, path, size, fit, unit);
            if (type == 'l' || type == 'L')
                crear_pl(name, path, size, fit, unit);
        } else {
            if (!p_name)
                printf("ERROR: para crear una particion se nesesita de un nombre.\n");
            if (!p_size)
                printf("ERROR: para crear una particion se nesesita de un tamano.\n");
            if (!p_path)
                printf("ERROR: para crear una particion se nesesita del directorio en donde esta el disco que la alojara.\n");
            return;
        }
    }


}

void mount(Montaje *mont, char *token) {
    int p_path = 0;
    int p_name = 0;

    char path[200];
    char *name;

    char valor[500];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);

                    valor_r = ncomillas(valor_r);

                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: el parametro 'path' esta declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    name = malloc(sizeof (char)*strlen(valor_r));
                    strcpy(name, valor_r);
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                p_name = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_path * p_name) {
        mount_c(mont, path, name);
    } else {
        if (!p_path) {
            printf("ERROR: falta el parametro 'path'.\n");
        }
        if (!p_name) {
            printf("ERROR: falta el parametro 'name'.\n");
        }
    }
    free(valor_r);
    free(name);
}

void umount(Montaje *mont, char *token) {
    char id[20];

    char valor[20];
    char *valor_r;
    int pos;
    int i;
    token = strtok(NULL, " ");
    while (token) {

        if (!pasa(token, 1, 0)) {
            printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
            return;
        }
        pos = pasa2(token);
        if (pos == 0) {
            printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
            return;
        }
        for (i = 0; token[i + pos] != '\0'; i++)
            valor[i] = token[i + pos];
        valor[i] = '\0';
        valor_r = valor_real(longitud_real(valor), valor);

        strcpy(id, valor_r);

        umount_c(mont, id);

        token = strtok(NULL, " ");
    }
}

void rep(Montaje *montaje, char *token) {
    int p_name = 0;
    int p_id = 0;
    int p_ruta = 0;
    int p_path = 0;

    char path[100];
    char ruta[100];
    char id[6];
    int name = 0;

    char valor[100];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);

                    valor_r = ncomillas(valor_r);

                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: el parametro 'path' esta declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    name = name_rep(valor_r);
                    if (name == 0) {
                        printf("ERROR: el valor para el parametro name no es valido.\n");
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                p_name = 1;
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 5] != '\0'; i++)
                        valor[i] = token[i + 5];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    strcpy(id, valor_r);
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                p_id = 1;
                break;
            case 10:
                if (!p_ruta) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    strcpy(ruta, valor_r);
                } else {
                    printf("ERROR: el parametro 'ruta' esta declarado mas de una vez.\n");
                    return;
                }
                p_ruta = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_path * p_name * p_id) {
        switch (name) {
            case 1:
                printf("-- GENERANDO REPORTE \"SB\" --\n");
                rep_mbr(montaje, id, path);
                break;
            case 2:
                printf("-- GENERANDO REPORTE \"disk\" --\n");
                rep_disk(montaje, id, path);
                break;
            case 3:
                printf("-- GENERANDO REPORTE \"inode\" --\n");
                rep_inode(montaje, id, path);
                break;
            case 4:
                printf("-- GENERANDO REPORTE \"journaling\" --\n");
                rep_journaling(montaje, id, path);
                break;
            case 5:
                printf("-- GENERANDO REPORTE \"block\" --\n");
                rep_block(montaje, id, path);
                break;
            case 6:
                printf("-- GENERANDO REPORTE \"SB\" --\n");
                rep_bm_inode(montaje, id, path);
                break;
            case 7:
                printf("-- GENERANDO REPORTE \"SB\" --\n");
                rep_bm_block(montaje, id, path);
                break;
            case 8:
                printf("-- GENERANDO REPORTE \"tree\" --\n");
                rep_tree(montaje, id, path);
                break;
            case 9:
                printf("-- GENERANDO REPORTE \"SB\" --\n");
                rep_sb(montaje, id, path);
                break;
            case 10:
                if (p_ruta) {
                    printf("-- Disculpa, reporte aun no valido --\n");
                    break;
                }
                printf("ERROR: el Parametro \"ruta\" no esta declarado.\n");
                break;
            case 11:
                if (p_ruta) {
                    printf("-- Disculpa, reporte aun no valido --\n");
                    break;
                }
                printf("ERROR: el Parametro \"ruta\" no esta declarado.\n");
                break;
            case 12:
                if (p_ruta) {
                    printf("-- Disculpa, reporte aun no valido --\n");
                    break;
                }
                printf("ERROR: el Parametro \"ruta\" no esta declarado.\n");
                break;
        }
    } else {
        if (!p_path)
            printf("ERROR: falta el parametro path.\n");
        if (!p_name)
            printf("ERROR: falta el parametro name.\n");
        if (!p_id)
            printf("ERROR: falta el parametro id.\n");
        return;
    }

}

void exec(char *token, Montaje *montaje, Sesion *sesion) {
    int p_path = 0;

    char path[200];

    char valor[500];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);

                    valor_r = ncomillas(valor_r);

                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: el parametro 'path' esta declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_path) {
        struct stat st = {0};
        if (stat(path, &st) == -1) {
            printf("ERROR: el archivo especificado no existe.\n");
            printf("-------------CARGA FALLIDA--------------\n\n");
            return;
        }

        FILE *execa;

        execa = fopen(path, "r");

        if (!execa) {
            printf("ERROR: el Archivo no ha podido Abrirse.\n");
            printf("-------------CARGA FALLIDA--------------\n\n");
            return;
        }

        long size;
        fseek(execa, 0, SEEK_END);
        size = ftell(execa) - 2;
        fseek(execa, 0, SEEK_SET);
        char contenido [size];
        fread(contenido, 1, size, execa);
        fclose(execa);
        char comando[100] = "";
        int recorrido = 0;
        int bandera = 0;
        for (int i = 0; i < size; i++) {
            if (contenido[i] == '\n' && !bandera) {
                printf("-- %s --\n", comando);
                if (comando[0] != '#') {
                    subMain(comando, montaje, sesion);
                }
                recorrido = 0;
                for (int i = 0; i < 100; i++)
                    comando[i] = '\0';
            } else if (contenido[i] == '\\' && contenido[i + 1] == '^') {
                bandera = 1;
                i = i + 2;
            } else if (contenido[i] == '\n' && bandera) {
                bandera = 0;
            } else {
                comando[recorrido] = contenido[i];
                recorrido++;
            }
        }
    }
}

void subMain(char *comando, Montaje *montaje, Sesion *sesion) {
    char *token;
    char cadena[strlen(comando)];
    strcpy(cadena, comando);
    token = strtok(cadena, " ");
    while (token) {
        switch (opcion(token)) {
            case 1:
                mkdisk(token);
                return;
            case 2:
                rmdisk(montaje, token);
                return;
            case 3:
                fdisk(token);
                return;
            case 4:
                mount(montaje, token);
                return;
            case 5:
                umount(montaje, token);
                return;
            case 6:
                rep(montaje, token);
                return;
            case 9:
                printf("COMANDO DF INVOCADO\n");
                return;
            case 10:
                printf("COMANDO DU INVOCADO\n");
                return;
            case 11:
                mkfs(montaje, token);
                return;
            case 12:
                if (sesion->UID != -1) {
                    printf("ERROR: Deve cerrar sesion primero antes de iniciar otra sesion.\n");
                } else {
                    clogin(montaje, sesion, token);
                }
                return;
            case 13:
                if (sesion->UID == -1) {
                    printf("ERROR: no hay ninguna Sesion Iniciada.\n");
                } else {
                    sesion->GID = -1;
                    sesion->UID = -1;
                    sesion->lista = -1;
                    sesion->montaje = -1;
                    printf("Sesion Cerrada.\n");
                }
                return;
            case 14:
                if (sesion->UID != 1) {
                    printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                } else {
                    mkgrp(montaje, sesion, token);
                }
                return;
            case 15:
                if (sesion->UID != 1) {
                    printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                } else {
                    rmgrp(montaje, sesion, token);
                }
                return;
            case 16:
                if (sesion->GID != 1) {
                    printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                } else {
                    mkusr(montaje, sesion, token);
                }
                return;
            case 17:
                if (sesion->UID != 1) {
                    printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                } else {
                    rmusr(montaje, sesion, token);
                }
                return;
            case 18:
                printf("COMANDO CHMOD INVOCADO\n");
                return;
            case 19:
                printf("COMANDO MKFILE INVOCADO\n");
                return;
            case 20:
                printf("COMANDO CAT INVOCADO\n");
                return;
            case 21:
                printf("COMANDO RM INVOCADO\n");
                return;
            case 22:
                printf("COMANDO EDIT INVOCADO\n");
                return;
            case 23:
                printf("COMANDO REN INVOCADO\n");
                return;
            case 24:
                if (sesion->UID == -1)
                    printf("ERROR: no hay ninguna Sesion Iniciada.\n");
                else
                    vmkdir(montaje, sesion, token);
                return;
            case 25:
                printf("COMANDO CP INVOCADO\n");
                return;
            case 26:
                printf("COMANDO MV INVOCADO\n");
                return;
            case 27:
                printf("COMANDO FIND INVOCADO\n");
                return;
            case 28:
                printf("COMANDO CHOWN INVOCADO\n");
                return;
            case 29:
                printf("COMANDO CHGRP INVOCADO\n");
                return;
            case 30:
                printf("COMANDO LN INVOCADO\n");
                return;
            case 31:
                printf("COMANDO UNLINK INVOCADO\n");
                return;
            case 32:
                printf("COMANDO TUNE2FS INVOCADO\n");
                return;
            case 33:
                printf("COMANDO RECOVERY INVOCADO\n");
                return;
            case 34:
                printf("COMANDO LOSS INVOCADO\n");
                return;
            default:
                printf("ERROR: el comando \"%s\" no reconocido\n", token);
                return;
        }
    }
}