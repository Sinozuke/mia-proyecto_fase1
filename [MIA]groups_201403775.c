#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]groups_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);
void obtener_cont_archivo(char *contenido, inodo *ino, FILE *disco, SB *sb);
int contar_saltos(char *cont);
void almacenar_registros(Registro *regis, char *cont);
void vaciar(Registro *registro, int numero);
void encontrar_id_anterior(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo);
void eliminar_id(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo);
void rtc(Registro *registro, int cantidad, char *dest, int longitud);
int contar_g_u(Registro *registros, int cantidad, char tipo);
int lon_contenido(Registro *registro, int cantidad);
void editar_archivo(FILE *disco, inodo *ino, SB *sb, char *cont);
void primer_b_f(FILE *disco, SB *sb);
void primer_i_f(FILE *disco, SB *sb);
void editar_archivo_nivel0(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);
void editar_archivo_nivel1(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);
void editar_archivo_nivel2(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);


void crear_grupo(Montaje *montaje, Sesion *sesion, char *nombre, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != '2' && encontrado.status != '3') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB sb;
    inodo ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fread(&ino, sizeof (inodo), 1, disco);

    char contenido[ino.i_size];
    strcpy(contenido, "");
    obtener_cont_archivo(contenido, &ino, disco, &sb);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros + 1];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    int bandera = 1;
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'g') {
            if (strcmp(registros[i].grupo, nombre) == 0) {
                if (registros[i].id != 0) {
                    printf("ERROR: El Grupo ya Exite.\n");
                    ino.i_atime = time(NULL);
                    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
                    fwrite(&ino, sizeof (inodo), 1, disco);
                    fclose(disco);
                    return;
                } else {
                    printf("- Reactivando Grupo \"%s\" -\n", nombre);
                    encontrar_id_anterior(registros, &registros[i].id, numero_registros, nombre,'g');
                    printf("- Reactivacion Completa -\n");
                    bandera = 0;
                }
            }
        }
    }

    if (bandera) {
        registros[numero_registros].id = contar_g_u(registros, numero_registros, 'g') + 1;
        strcpy(registros[numero_registros].grupo, nombre);
        registros[numero_registros].tipo = 'g';
    }
    int conte = lon_contenido(registros, numero_registros);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros, nuevo_contenido, conte);

    ino.i_size=conte;
    editar_archivo(disco, &ino, &sb, nuevo_contenido);

    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&sb, sizeof (SB), 1, disco);

    fclose(disco);
    printf("Creacion Completa.\n");
}

void editar_archivo(FILE *disco, inodo *ino, SB *sb, char *cont) {
    int longitud = strlen(cont);
    int recorrido = 0;
    char vacio = '0';
    char apunta = '3';
    char arch = 'f';
    bloque_archivo archivo;
    for (int i = 0; i < 12 && recorrido != longitud; i++) {
        if (ino->i_block[i] != -1) {
            if (recorrido != longitud) {
                fseek(disco, sb->s_block_start + ino->i_block[i] * sizeof (bloque_archivo), SEEK_SET);
                fread(&archivo, sizeof (bloque_archivo), 1, disco);
                for (int y = 0; y < 64; y++)
                    archivo.b_content[y] = '\0';
                for (int j = 0; cont[recorrido] && j < 64; j++) {
                    archivo.b_content[j] = cont[recorrido];
                    recorrido++;
                }
                fseek(disco, sb->s_block_start + ino->i_block[i] * sizeof (bloque_archivo), SEEK_SET);
                fwrite(&archivo, sizeof (bloque_archivo), 1, disco);
            } else {
                fseek(disco, sb->s_bm_block_start + ino->i_block[i] * sizeof (bloque_archivo), SEEK_SET);
                fwrite(&vacio, sizeof (char), 1, disco);
                if (sb->s_first_blo > ino->i_block[i])
                    sb->s_first_blo = ino->i_block[i];
                ino->i_block[i] = -1;
                sb->s_free_blocks_count++;
            }
        } else {
            if (recorrido != longitud) {
                for (int y = 0; y < 64; y++)
                    archivo.b_content[y] = '\0';
                for (int j = 0; cont[recorrido] && j < 64; j++) {
                    archivo.b_content[j] = cont[recorrido];
                    recorrido++;
                }
                fseek(disco, sb->s_block_start + sb->s_first_blo * sizeof (bloque_archivo), SEEK_SET);
                fwrite(&archivo, sizeof (bloque_archivo), 1, disco);
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&arch, sizeof (char), 1, disco);
                ino->i_block[i]=sb->s_first_blo;
                primer_b_f(disco, sb);
                sb->s_free_blocks_count--;
            }
        }
    }

    bloque_apuntadores apunt;

    if (ino->i_block[12] != -1) {
        fseek(disco, sb->s_block_start + ino->i_block[12] * sizeof (bloque_apuntadores), SEEK_SET);
        fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
        editar_archivo_nivel0(disco, &apunt, sb, &recorrido, longitud, cont);
        if (apunt.b_pointers[0] == -1) {
            fseek(disco, sb->s_bm_block_start + ino->i_block[12] * sizeof (char), SEEK_SET);
            fwrite(&vacio, sizeof (char), 1, disco);
            sb->s_free_blocks_count++;
            if (ino->i_block[12] < sb->s_first_blo)
                sb->s_first_blo = ino->i_block[12];
        } else {
            fseek(disco, sb->s_block_start + ino->i_block[12] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        }
    } else {
        if (recorrido != longitud) {
            for (int i = 0; i < 16; i++)
                apunt.b_pointers[i] = -1;
            ino->i_block[12] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apunta, sizeof (char), 1, disco);
            sb->s_free_blocks_count--;
            primer_b_f(disco, sb);
            editar_archivo_nivel0(disco, &apunt, sb, &recorrido, longitud, cont);
            fseek(disco, sb->s_block_start + ino->i_block[12] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        } else {
            return;
        }
    }

    if (ino->i_block[13] != -1) {
        fseek(disco, sb->s_block_start + ino->i_block[13] * sizeof (bloque_apuntadores), SEEK_SET);
        fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
        editar_archivo_nivel1(disco, &apunt, sb, &recorrido, longitud, cont);
        if (apunt.b_pointers[0] == -1) {
            fseek(disco, sb->s_bm_block_start + ino->i_block[13] * sizeof (char), SEEK_SET);
            fwrite(&vacio, sizeof (char), 1, disco);
            sb->s_free_blocks_count++;
            if (ino->i_block[13] < sb->s_first_blo)
                sb->s_first_blo = ino->i_block[13];
        } else {
            fseek(disco, sb->s_block_start + ino->i_block[13] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        }
    } else {
        if (recorrido != longitud) {
            for (int i = 0; i < 16; i++)
                apunt.b_pointers[i] = -1;
            ino->i_block[13] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apunta, sizeof (char), 1, disco);
            sb->s_free_blocks_count--;
            primer_b_f(disco, sb);
            editar_archivo_nivel1(disco, &apunt, sb, &recorrido, longitud, cont);
            fseek(disco, sb->s_block_start + ino->i_block[13] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        } else {
            return;
        }
    }

    if (ino->i_block[14] != -1) {
        fseek(disco, sb->s_block_start + ino->i_block[14] * sizeof (bloque_apuntadores), SEEK_SET);
        fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
        editar_archivo_nivel2(disco, &apunt, sb, &recorrido, longitud, cont);
        if (apunt.b_pointers[0] == -1) {
            fseek(disco, sb->s_bm_block_start + ino->i_block[14] * sizeof (char), SEEK_SET);
            fwrite(&vacio, sizeof (char), 1, disco);
            sb->s_free_blocks_count++;
            if (ino->i_block[14] < sb->s_first_blo)
                sb->s_first_blo = ino->i_block[14];
        } else {
            fseek(disco, sb->s_block_start + ino->i_block[14] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        }
    } else {
        if (recorrido != longitud) {
            for (int i = 0; i < 16; i++)
                apunt.b_pointers[i] = -1;
            ino->i_block[14] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apunta, sizeof (char), 1, disco);
            sb->s_free_blocks_count--;
            primer_b_f(disco, sb);
            editar_archivo_nivel2(disco, &apunt, sb, &recorrido, longitud, cont);
            fseek(disco, sb->s_block_start + ino->i_block[14] * sizeof (char), SEEK_SET);
            fwrite(&apunt, sizeof (bloque_apuntadores), 1, disco);
        } else {
            return;
        }
    }

    ino->i_size = longitud;
    ino->i_mtime = time(NULL);

}

void editar_archivo_nivel0(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido) {
    bloque_archivo archivo;
    char arch = 'f';
    char vacio = '0';
    for (int i = 0; i < 16; i++) {
        if (apunt->b_pointers[i] != -1) {
            if (*recorrido != longitud) {
                fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_archivo), SEEK_SET);
                fread(&archivo, sizeof (bloque_archivo), 1, disco);
                for (int j = 0; j < 64; j++)
                    archivo.b_content[j] = '\0';
                for (int j = 0; *recorrido != longitud; j++) {
                    archivo.b_content[j] = contenido[*recorrido];
                    recorrido++;
                }
                fseek(disco, -sizeof (bloque_archivo), SEEK_CUR);
                fread(&archivo, sizeof (bloque_archivo), 1, disco);
            } else {
                fseek(disco, sb->s_bm_block_start + apunt->b_pointers[i] * sizeof (char), SEEK_SET);
                fwrite(&vacio, sizeof (char), 1, disco);
                if (apunt->b_pointers[i] < sb->s_first_blo)
                    sb->s_first_blo = apunt->b_pointers[i];
                sb->s_free_blocks_count++;
                apunt->b_pointers[i] = -1;
            }
        } else {
            if (*recorrido != longitud) {
                for (int j = 0; j < 64; j++)
                    archivo.b_content[j] = '\0';
                for (int j = 0; *recorrido != longitud; j++) {
                    archivo.b_content[j] = contenido[*recorrido];
                    recorrido++;
                }
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&arch, sizeof (char), 1, disco);
                fseek(disco, sb->s_block_start + sb->s_first_blo * sizeof (bloque_archivo), SEEK_SET);
                fwrite(&archivo, sizeof (bloque_archivo), 1, disco);
                apunt->b_pointers[i] = sb->s_first_blo;
                primer_b_f(disco, sb);
            }
        }
    }

}

void editar_archivo_nivel1(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido) {
    bloque_apuntadores apuntadores;
    char vacio = '0';
    char apunta = 'a';
    for (int i = 0; i < 16; i++) {
        if (apunt->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            editar_archivo_nivel0(disco, &apuntadores, sb, recorrido, longitud, contenido);
            if (apuntadores.b_pointers[0] == -1) {
                fseek(disco, sb->s_bm_block_start + apunt->b_pointers[i] * sizeof (char), SEEK_SET);
                fwrite(&vacio, sizeof (char), 1, disco);
                if (apunt->b_pointers[i] < sb->s_first_blo)
                    sb->s_first_blo = apunt->b_pointers[i];
                sb->s_free_blocks_count++;
                apunt->b_pointers[i] = -1;
            } else {
                fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
                fwrite(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            }
        } else {
            if (*recorrido != longitud) {
                for (int i = 0; i < 16; i++)
                    apuntadores.b_pointers[i] = -1;
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&apunta, sizeof (char), 1, disco);
                apunt->b_pointers[i] = sb->s_first_blo;
                primer_b_f(disco, sb);
                sb->s_free_blocks_count--;
                editar_archivo_nivel0(disco, &apuntadores, sb, recorrido, longitud, contenido);
                fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
                fwrite(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            } else {
                return;
            }
        }
    }
}

void editar_archivo_nivel2(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido) {
    bloque_apuntadores apuntadores;
    char vacio = '0';
    char apunta = 'a';
    for (int i = 0; i < 16; i++) {
        if (apunt->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            editar_archivo_nivel1(disco, &apuntadores, sb, recorrido, longitud, contenido);
            if (apuntadores.b_pointers[0] == -1) {
                fseek(disco, sb->s_bm_block_start + apunt->b_pointers[i] * sizeof (char), SEEK_SET);
                fwrite(&vacio, sizeof (char), 1, disco);
                if (apunt->b_pointers[i] < sb->s_first_blo)
                    sb->s_first_blo = apunt->b_pointers[i];
                sb->s_free_blocks_count++;
                apunt->b_pointers[i] = -1;
            } else {
                fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
                fwrite(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            }
        } else {
            if (*recorrido != longitud) {
                for (int i = 0; i < 16; i++)
                    apuntadores.b_pointers[i] = -1;
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&apunta, sizeof (char), 1, disco);
                apunt->b_pointers[i] = sb->s_first_blo;
                primer_b_f(disco, sb);
                sb->s_free_blocks_count--;
                editar_archivo_nivel1(disco, &apuntadores, sb, recorrido, longitud, contenido);
                fseek(disco, sb->s_block_start + apunt->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
                fwrite(&apuntadores, sizeof (bloque_apuntadores), 1, disco);
            } else {
                return;
            }
        }
    }
}

void primer_b_f(FILE *disco, SB *sb) {
    char leido = '-';
    for (int i = 0; i < sb->s_blocks_count; i++) {
        fseek(disco, sb->s_bm_block_start + i * sizeof (char), SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido == '0'){
            sb->s_first_blo = i;
            return;
        }
    }
}

void primer_i_f(FILE *disco, SB *sb) {
    char leido = '-';
    for (int i = 0; i < sb->s_inodes_count; i++) {
        fseek(disco, sb->s_bm_inode_start + i * sizeof (char), SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido == '0'){
            sb->s_first_ino = i;
            return;
        }
    }
}


int lon_contenido(Registro *registro, int cantidad) {
    int devolver = 0;

    for (int i = 0; i <= cantidad; i++) {
        if (registro[i].tipo != '-') {
            char buffer[20];
            sprintf(buffer, "%d", registro[i].id);
            devolver += strlen(buffer);
            devolver += sizeof (char);
            devolver += sizeof (char);
            devolver += sizeof (char);
            devolver += strlen(registro[i].grupo);

            if (registro[i].tipo == 'u') {
                devolver += sizeof (char);
                devolver += strlen(registro[i].password);
                devolver += sizeof (char);
                devolver += strlen(registro[i].username);
            }
            devolver += sizeof (char);
        }
    }

    return devolver;
}

void rtc(Registro *registro, int cantidad, char *dest, int longitud) {
    for (int i = 0; i < longitud; i++)
        dest[i] = '\0';
    for (int i = 0; i <= cantidad; i++) {
        if (registro[i].tipo != '-') {
            char buffer[20];
            sprintf(buffer, "%d", registro[i].id);
            strcat(dest, buffer);
            strcat(dest, ",");
            dest[strlen(dest)] = registro[i].tipo;
            strcat(dest, ",");
            strcat(dest, registro[i].grupo);

            if (registro[i].tipo == 'u') {
                strcat(dest, ",");
                strcat(dest, registro[i].username);
                strcat(dest, ",");
                strcat(dest, registro[i].password);
            }
            strcat(dest, "\n");
        }
    }
}

int contar_g_u(Registro *registros, int cantidad, char tipo) {
    int devolver = 0;
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo)
            devolver++;
    }
    return devolver;
}

void encontrar_id_anterior(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo) {
    int numero = 0;
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo) {
            numero++;
            if(tipo=='u'){
                if (strcmp(registros[i].username, nombre) == 0) {
                    *retorno = numero;
                    return;
                }        
            }else{
                if (strcmp(registros[i].grupo, nombre) == 0) {
                    *retorno = numero;
                    return;
                }
            }
        }
    }
}

void eliminar_id(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo){
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo) {
            if(tipo=='u'){
                if (strcmp(registros[i].username, nombre) == 0) {
                    *retorno = 0;
                    return;
                }
            }else{
                if (strcmp(registros[i].grupo, nombre) == 0) {
                    *retorno = 0;
                    return;
                }
            }
        }
    }
}

void eliminar_grupo(Montaje *montaje, Sesion *sesion, char *nombre, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != '2' && encontrado.status != '3') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB sb;
    inodo ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fread(&ino, sizeof (inodo), 1, disco);

    char contenido[ino.i_size];
    strcpy(contenido, "");
    obtener_cont_archivo(contenido, &ino, disco, &sb);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    int bandera = 1;
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'g') {
            if (strcmp(registros[i].grupo, nombre) == 0) {
                if (registros[i].id != 0) {
                    eliminar_id(registros, &registros[i].id, numero_registros, nombre,'g');
                    bandera =0;
                } else {
                    printf("ERROR: El Grupo ya se ha eliminado.\n");
                    ino.i_atime = time(NULL);
                    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
                    fwrite(&ino, sizeof (inodo), 1, disco);
                    fclose(disco);
                    return;
                }
            }
        }
    }
    
    if(bandera){
        printf("ERROR: no se ha encontrado ningun grupo como  \"%s\"\n",nombre);
        ino.i_atime = time(NULL);
        fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
        fwrite(&ino, sizeof (inodo), 1, disco);
        fclose(disco);
        return;
    }

    int conte = lon_contenido(registros, numero_registros-1);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros-1, nuevo_contenido, conte);

    ino.i_size=conte;
    editar_archivo(disco, &ino, &sb, nuevo_contenido);

    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&sb, sizeof (SB), 1, disco);

    fclose(disco);
    printf("Eliminacion Completa.\n");

}

void vaciar(Registro *registro, int numero) {
    for (int i = 0; i < numero; i++) {
        for (int j = 0; j < 10; j++) {
            registro[i].grupo[j] = '\0';
            registro[i].username[j] = '\0';
            registro[i].password[j] = '\0';
        }
        registro[i].id = -1;
        registro[i].tipo = '-';
    }

}

void almacenar_registros(Registro *regis, char *cont) {
    char copia[strlen(cont)];
    strcpy(copia, "");
    strcpy(copia, cont);
    char *token;
    char registro[40];
    for (int i = 0; i < 40; i++) {
        registro[i] = '\0';
    }
    int conteo = 0, conteo2 = 0;
    int j = 0;
    for (int i = 0; copia[i] != '\0'; i++) {
        if (copia[i] != '\n') {
            registro[j] = copia[i];
            j++;
        } else {
            token = strtok(registro, ",");
            while (token) {
                switch (conteo) {
                    case 0:
                        regis[conteo2].id = atoi(token);
                        break;
                    case 1:
                        regis[conteo2].tipo = token[0];
                        break;
                    case 2:
                        strcpy(regis[conteo2].grupo, token);
                        break;
                    case 3:
                        strcpy(regis[conteo2].username, token);
                        break;
                    case 4:
                        strcpy(regis[conteo2].password, token);
                        break;
                }
                conteo++;
                token = strtok(NULL, ",");
            }
            for (int b = 0; b < 20; b++) {
                registro[b] = '\0';
            }
            conteo = 0;
            conteo2++;
            j = 0;
        }
    }
}

int contar_saltos(char *cont) {
    int resultado = 0;

    for (int i = 0; cont[i] != '\0'; i++)
        if (cont[i] == '\n')
            resultado++;

    return resultado;
}