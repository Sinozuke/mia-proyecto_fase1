#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]comandos_201403775.h"
#include "[MIA]Aux_comandos_201403775.h"
#include "[MIA]funciones_201403775.h"

void mkfs(Montaje *montaje, char* token) {
    int p_id = 0;
    int p_type = 0;
    int p_add = 0;
    int p_unit = 0;
    int p_fs = 0;

    char id[10];
    char type = 'u';
    int add = 0;
    int unit = 2;
    int ext = 3;

    char valor[100];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 2:
                if (!p_unit) {
                    if (!p_unit) {
                        if (!pasa(token, 1, 0)) {
                            printf("ERROR: no se presenta el separador '::' entre el parametro 'unit' y el valor.\n");
                            return;
                        }
                        for (i = 0; token[i + 7] != '\0'; i++)
                            valor[i] = token[i + 7];
                        valor[i] = '\0';
                        valor_r = valor_real(longitud_real(valor), valor);
                        unit = k_m(valor_r);
                        if (unit == 0) {
                            printf("el valor \"%s\" para el parametro unit no es aceptado.\n", valor_r);
                            return;
                        }
                    } else {
                        printf("ERROR: Parametro 'unit' declarado mas de una vez.\n");
                        return;
                    }
                    p_unit = 1;
                } else {
                    printf("ERROR: el parametro 'unit' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 5:
                if (!p_type) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'type' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    type = Fast_Full(valor_r);

                    if (type == 'n') {
                        printf("ERROr el valor \"%s\" para el parametro 'delete' no es valido.\n", valor_r);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'type' esta declarado mas de una vez.\n");
                    return;
                }
                p_type = 1;
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    int pos = pasa2(token);
                    if (pos == 0) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + pos] != '\0'; i++)
                        valor[i] = token[i + pos];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);

                    strcpy(id, valor_r);
                    for (int i = 0; id[i]; i++)
                        id[i] = tolower(id[i]);
                    p_id = 1;
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 11:
                if (!p_fs) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    int pos = pasa2(token);
                    if (pos == 0) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + pos] != '\0'; i++)
                        valor[i] = token[i + pos];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);

                    ext = ext2_ext3(valor_r);

                    if (ext == -1) {
                        printf("ERROR: el valor para el parametro de fs no es valido.\n");
                        printf("------------Formateo Fallido-------------\n");
                        return;
                    }
                    p_fs = 1;
                } else {
                    printf("ERROR: el parametro 'fs' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 8:
                if (!p_add) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'delete' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (!isnumero(valor_r)) {
                        printf("ERROR el Valor ingresado para size no es un numero.\n");
                        return;
                    }
                    add = atoi(valor_r);
                    if (add == 0) {
                        printf("ERROr el valor \"%i\" para el parametro 'add' no es valido.\n", add);
                        return;
                    }
                } else {
                    printf("ERROR: el parametro 'add' esta declarado mas de una vez.\n");
                    return;
                }
                p_add = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_add) {
        if (p_id) {
            if (p_type)
                printf("ADVERTENCIA: EL PARAMETRO \"type\" HA DE IGNORARSE PARA ESTA OPERACION\n.");
            if (p_fs)
                printf("ADVERTENCIA: EL PARAMETRO \"fs\" HA DE IGNORARSE PARA ESTA OPERACION\n.");
            add_formato(montaje, id, add, unit);
        } else {
            printf("ERROR: no se ha especificado ningun punto de montaje para realizar el formateo.\n");
            printf("-----------FORMATEO FALLIDO----------");
            return;
        }
    } else if (p_id) {
        if (p_unit)
            printf("ADVERTENCIA: EL PARAMETRO \"unit\" HA DE IGNORARSE PARA ESTA OPERACION\n.");
        formatear_mkfs(montaje, id, type, add, unit, ext);
    } else {
        printf("ERROR: no se ha especificado ningun punto de montaje para realizar el formateo.\n");
        printf("-----------FORMATEO FALLIDO----------");
    }
}

void mkgrp(Montaje *montaje, Sesion *sesion, char *token) {

    int p_name = 0;
    int p_id = 0;


    char id[6];
    char name[10];

    char valor[50];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);

                    if (strlen(valor_r) > 10 || strlen(valor_r) == 0) {
                        printf("ERROR: el valor ingresado para \"name\" es demaciado largo (MAX 10).\n");
                        return;
                    }

                    strcpy(name, valor_r);
                    p_name = 1;
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 5] != '\0'; i++)
                        valor[i] = token[i + 5];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    strcpy(id, valor_r);
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                p_id = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_id * p_name) {
        crear_grupo(montaje, sesion, name, id);
    } else {
        if (!p_id)
            printf("ERROR: no se especifico el parametro \"id\".\n");
        if (!p_name)
            printf("ERROR: no se especifico el parametro \"name\".\n");
    }
}

void rmgrp(Montaje *montaje, Sesion *sesion, char *token) {

    int p_name = 0;
    int p_id = 0;


    char id[6];
    char name[10];

    char valor[50];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 4:
                if (!p_name) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);

                    if (strlen(valor_r) > 10 || strlen(valor_r) == 0) {
                        printf("ERROR: el valor ingresado para \"name\" es demaciado largo (MAX 10).\n");
                        return;
                    }

                    strcpy(name, valor_r);
                    p_name = 1;
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 5] != '\0'; i++)
                        valor[i] = token[i + 5];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    strcpy(id, valor_r);
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                p_id = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_id * p_name) {
        eliminar_grupo(montaje, sesion, name, id);
    } else {
        if (!p_id)
            printf("ERROR: no se especifico el parametro \"id\".\n");
        if (!p_name)
            printf("ERROR: no se especifico el parametro \"name\".\n");
    }
}

void vmkdir(Montaje *montaje, Sesion *sesion, char* token) {

    int p = 0;
    char id[10] = "";
    char *path;

    int p_p = 0;
    int p_id = 0;
    int p_path = 0;


    char valor[100];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    int pos = pasa2(token);
                    if (pos == 0) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + pos] != '\0'; i++)
                        valor[i] = token[i + pos];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);

                    strcpy(id, valor_r);
                    for (int i = 0; id[i]; i++)
                        id[i] = tolower(id[i]);
                    p_id = 1;
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 3:
                if (!p_path) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 7] != '\0'; i++)
                        valor[i] = token[i + 7];
                    valor[i] = '\0';
                    while (!pasa(valor, 0, 1)) {
                        token = strtok(NULL, " ");
                        if (!token) {
                            printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                            return;
                        }
                        strcat(valor, " ");
                        strcat(valor, token);
                    }
                    valor_r = valor_real(longitud_real(valor), valor);
                    valor_r = ncomillas(valor_r);
                    path = malloc(sizeof (char)*strlen(valor_r));
                    strcpy(path, valor_r);
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                p_path = 1;
                break;
            case 12:
                if (!p_p) {
                    p_p = 1;
                    p = 1;
                } else {
                    printf("ERROR: Parametro 'p' declarado mas de una vez.\n");
                    return;
                }
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_id * p_path) {
        crear_Directorio(montaje,sesion, p, id, path, 664);
        return;
    }
    if (!p_id)
        printf("ERROR: No se ha especificado el parametro \"id\".\n");

    if (!p_path)
        printf("ERROR: No se ha especificado el parametro \"path\".\n");

}

void clogin(Montaje *montaje, Sesion *sesion, char *token) {

    int p_id = 0;
    int p_username = 0;
    int p_pass = 0;

    char id[10];
    char username[10];
    char password[10];

    char valor[100];
    char *valor_r;
    int i;

    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    int pos = pasa2(token);
                    if (pos == 0) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + pos] != '\0'; i++)
                        valor[i] = token[i + pos];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);

                    strcpy(id, valor_r);
                    for (int i = 0; id[i]; i++)
                        id[i] = tolower(id[i]);
                    p_id = 1;
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 13:
                if (!p_username) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);
                    if (strlen(valor_r) > 11) {
                        printf("ERROR: el valor para el password es demaciado largo, max:10.\n");
                        return;
                    }
                    strcpy(username, valor_r);
                    p_username = 1;
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                break;
            case 14:
                if (!p_pass) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);
                    if (strlen(valor_r) > 11) {
                        printf("ERROR: el valor para el password es demaciado largo, max:10.\n");
                        return;
                    }
                    strcpy(password, valor_r);
                    p_pass = 1;
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_username * p_pass) {
        login(montaje, sesion, username, password, id, p_id);
    } else {
        if (!p_username)
            printf("ERROR: parametro \"usr\" (Username) no fue declarado.\n");
        if (!p_pass)
            printf("ERROR: parametro \"pwd\" (Password) no fue declarado.\n");
    }



}

void mkusr(Montaje *montaje, Sesion *sesion, char *token){
    int p_grp = 0;
    int p_id = 0;
    int p_username = 0;
    int p_pass = 0;


    char id[6];
    char username[10];
    char password[10];
    char group[10];

    char valor[50];
    char *valor_r;
    int i;


    
    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 13:
                if (!p_username) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);
                    if (strlen(valor_r) > 11) {
                        printf("ERROR: el valor para el password es demaciado largo, max:10.\n");
                        return;
                    }
                    strcpy(username, valor_r);
                    p_username = 1;
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                break;
            case 14:
                if (!p_pass) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);
                    if (strlen(valor_r) > 11) {
                        printf("ERROR: el valor para el password es demaciado largo, max:10.\n");
                        return;
                    }
                    strcpy(password, valor_r);
                    p_pass = 1;
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                break;
            case 15:
                if (!p_grp) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);

                    if (strlen(valor_r) > 10 || strlen(valor_r) == 0) {
                        printf("ERROR: el valor ingresado para \"grp\" es demaciado largo (MAX 10).\n");
                        return;
                    }

                    strcpy(group, valor_r);
                    p_grp = 1;
                } else {
                    printf("ERROR: el parametro 'name' esta declarado mas de una vez.\n");
                    return;
                }
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 5] != '\0'; i++)
                        valor[i] = token[i + 5];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    strcpy(id, valor_r);
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                p_id = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_id * p_grp*p_username*p_pass) {
        crear_usuario(montaje,sesion,username,group,password,id);
    } else {
        if (!p_id)
            printf("ERROR: no se especifico el parametro \"id\".\n");
        if (!p_grp)
            printf("ERROR: no se especifico el parametro \"grp\".\n");
        if (!p_username)
            printf("ERROR: no se especifico el parametro \"usr\".\n");
        if (!p_pass)
            printf("ERROR: no se especifico el parametro \"pwd\".\n");
    }




}


void rmusr(Montaje *montaje, Sesion *sesion, char *token){
    int p_id = 0;
    int p_username = 0;


    char id[6];
    char username[10];

    char valor[50];
    char *valor_r;
    int i;
    
    token = strtok(NULL, " ");
    while (token) {
        switch (parametros(token)) {
            case 13:
                if (!p_username) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'path' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 6] != '\0'; i++)
                        valor[i] = token[i + 6];
                    valor[i] = '\0';
                    if (valor[0] == '\"')
                        while (!pasa(valor, 0, 1)) {
                            token = strtok(NULL, " ");
                            if (!token) {
                                printf("ERROR: se ha llegado al final de lectura sin terminar el valor con la doble comillas.\n");
                                return;
                            }
                            strcat(valor, " ");
                            strcat(valor, token);
                        }
                    valor_r = valor_real(longitud_real(valor), valor);
                    if (valor_r[0] == '\"')
                        valor_r = ncomillas(valor_r);
                    if (strlen(valor_r) > 11) {
                        printf("ERROR: el valor para el password es demaciado largo, max:10.\n");
                        return;
                    }
                    strcpy(username, valor_r);
                    p_username = 1;
                } else {
                    printf("ERROR: Parametro 'path' declarado mas de una vez.\n");
                    return;
                }
                break;
            case 9:
                if (!p_id) {
                    if (!pasa(token, 1, 0)) {
                        printf("ERROR: no se presenta el separador '::' entre el parametro 'name' y el valor.\n");
                        return;
                    }
                    for (i = 0; token[i + 5] != '\0'; i++)
                        valor[i] = token[i + 5];
                    valor[i] = '\0';
                    valor_r = valor_real(longitud_real(valor), valor);
                    strcpy(id, valor_r);
                } else {
                    printf("ERROR: el parametro 'id' esta declarado mas de una vez.\n");
                    return;
                }
                p_id = 1;
                break;
            default:
                printf("Parametro \"%s\" NO reconocido.\n", token);
                return;
        }
        token = strtok(NULL, " ");
    }
    if (p_id*p_username) {
        eliminar_usuario(montaje,sesion,username,id);
    } else {
        if (!p_id)
            printf("ERROR: no se especifico el parametro \"id\".\n");
        if (!p_username)
            printf("ERROR: no se especifico el parametro \"usr\".\n");
    }




}