#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]mkfs_201403775.h"
#include "[MIA]Directorios_201403775.h"

void formatear_mkfs(Montaje *montaje, char *id, char type, int add, int unit, int ext) {
    char path[100];
    char byte = '\0';

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }
    strcpy(path, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    printf("-------------COMENZADO FORMATEO----------------\n");

    int n;

    if (ext == 2)
        n = (encontrado.size - sizeof (SB)) / (4 + sizeof (inodo) + 3 * sizeof (bloque_archivo));
    else
        n = (encontrado.size - sizeof (SB)) / (4 + sizeof (Journal) + sizeof (inodo) + 3 * sizeof (bloque_archivo));

    SB super_bloque;

    super_bloque.s_file_system_type = ext;
    super_bloque.s_inodes_count = n;
    super_bloque.s_blocks_count = 3 * n;
    super_bloque.s_free_blocks_count = 3 * n;
    super_bloque.s_free_inodes_count = n;
    super_bloque.s_mnt_count = 1;
    super_bloque.s_magic = 0xEF53;
    super_bloque.s_inode_size = sizeof (inodo);
    super_bloque.s_block_size = sizeof (bloque_archivo);
    super_bloque.s_first_ino = 0;
    super_bloque.s_first_blo = 0;

    if (ext == 2)
        super_bloque.s_bm_inode_start = encontrado.comienzo + sizeof (SB);
    else
        super_bloque.s_bm_inode_start = encontrado.comienzo + sizeof (SB) + sizeof (Journal);

    super_bloque.s_bm_block_start = super_bloque.s_bm_inode_start + n;
    super_bloque.s_inode_start = super_bloque.s_bm_block_start + 3 * n;
    super_bloque.s_block_start = super_bloque.s_inode_start + n * sizeof (inodo);

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    if (encontrado.status == 's') {
        if (encontrado.tipo == 'e') {
            printf("ERROR:el tipo de particion montada es EXTENDIDA.\nNo se puede aplicar formato a este tipo de particion.\n");
            printf("-------------FORMATEO FALLIDO--------------\n\n");
            fclose(disco);
            return;
        }
        if (encontrado.tipo == 'p') {
            MBR mbr;

            if (fread(&mbr, sizeof (MBR), 1, disco) != 1) {
                printf("ERROR:al cargar la data del disco\n");
                printf("-------------CREACION FALLIDA--------------\n\n");
                fclose(disco);
                return;
            }

            if (mbr.mbr_partition_1.part_start == encontrado.comienzo)
                mbr.mbr_partition_1.part_status = ext + '0';
            else if (mbr.mbr_partition_2.part_start == encontrado.comienzo)
                mbr.mbr_partition_2.part_status = ext + '0';
            else if (mbr.mbr_partition_3.part_start == encontrado.comienzo)
                mbr.mbr_partition_3.part_status = ext + '0';
            else
                mbr.mbr_partition_4.part_status = ext + '0';

            fseek(disco, 0, SEEK_SET);
            fwrite(&mbr, sizeof (MBR), 1, disco);
            encontrado.tipo = ext + '0';
            montaje[id[2] - 97].lista[a] = encontrado;
        } else {
            fseek(disco, encontrado.comienzo, SEEK_SET);
            fseek(disco, -sizeof (EBR), SEEK_CUR);
            EBR ebr;

            if (fread(&ebr, sizeof (EBR), 1, disco) != 1) {
                printf("ERROR:al cargar la data del disco\n");
                printf("-------------FORMATEO FALLIDO--------------\n\n");
                fclose(disco);
                return;
            }

            ebr.part_status = ext + '0';
            fseek(disco, -sizeof (EBR), SEEK_CUR);
            fwrite(&ebr, sizeof (EBR), 1, disco);
        }
    } else {
        printf("ADVERTENCIA: la particion montada ya contiene un sistema de archivos, desea formatear de iogual forma?(S/n)\n");
        char res;
        fgets(&res, sizeof (char), stdin);
        while (res != 'n' && res != 'S') {
            printf("Respuesta no Valida.(S/n)\n");
            fgets(&res, 10, stdin);
        }

        if (res == 'n') {
            printf("-----------FORMATEO CANCELADO------------\n");
            return;
        }
    }
    if (type == 'u') {
        fseek(disco, encontrado.comienzo, SEEK_SET);
        for (int i = 0; i < encontrado.size; i++) {
            fwrite(&byte, sizeof (char), 1, disco);
        }
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&super_bloque, sizeof (SB), 1, disco);
    Journal journaling;
    inodo ino;
    bloque_carpeta bloque;

    journaling.Journal_Permisos = 0;
    strcpy(journaling.Journal_contenido, "");
    ctime(&journaling.Journal_fecha);
    strcpy(journaling.Journal_nombre, "");
    strcpy(journaling.Journal_propietario, "");
    journaling.Journal_tipo = '-';
    journaling.Journal_tipo_operacion = '-';

    if (ext == 3)
        for (int i = 0; i < super_bloque.s_inodes_count; i++) {
            fwrite(&journaling, sizeof (Journal), 1, disco);
        }

    char vacio = '0';
    fseek(disco, super_bloque.s_bm_inode_start, SEEK_SET);
    for (int i = 0; i < n; i++)
        fwrite(&vacio, sizeof (char), 1, disco);
    fseek(disco, super_bloque.s_bm_block_start, SEEK_SET);
    for (int i = 0; i < 3 * n; i++)
        fwrite(&vacio, sizeof (char), 1, disco);
    fseek(disco, super_bloque.s_inode_start, SEEK_SET);
    for (int i = 0; i < n; i++)
        fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, super_bloque.s_block_start, SEEK_SET);
    for (int i = 0; i < 3 * n; i++)
        fwrite(&bloque, sizeof (bloque_carpeta), 1, disco);
    fclose(disco);
    printf("----------FORMATEO COMPLETO------------\n");
    SB sb;
    printf("Creando la carpeta raiz (/)\n");

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("\n");
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    fseek(disco, 0, SEEK_SET);
    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    inodo ino_nuevo;

    ino_nuevo.I_gid = 1;
    ino_nuevo.i_atime = time(NULL);
    for (int i = 0; i < 15; i++)
        ino_nuevo.i_block[i] = -1;
    ino_nuevo.i_block[0] = 0;
    ino_nuevo.i_ctime = time(NULL);
    ino_nuevo.i_mtime = time(NULL);
    ino_nuevo.i_nlink = 0;
    strcpy(ino_nuevo.i_pathlink, "/");
    ino_nuevo.i_perm = 777;
    ino_nuevo.i_size = 0;
    ino_nuevo.i_type = '1';
    ino_nuevo.i_uid = 1;

    sb.s_first_blo++;
    sb.s_first_ino++;
    sb.s_free_blocks_count--;
    sb.s_free_inodes_count--;

    bloque_carpeta raiz;

    raiz.b_content[0].b_inodo = 0;
    strcpy(raiz.b_content[0].bname, "..");
    raiz.b_content[1].b_inodo = 0;
    strcpy(raiz.b_content[1].bname, "/");
    raiz.b_content[2].b_inodo = -1;
    strcpy(raiz.b_content[2].bname, "-");
    raiz.b_content[3].b_inodo = -1;
    strcpy(raiz.b_content[3].bname, "-");

    char lleno = '1';
    char carpeta = 'd';
    char archivo = 'f';

    fseek(disco, -sizeof (SB), SEEK_CUR);
    fwrite(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_bm_inode_start, SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);
    fseek(disco, sb.s_bm_block_start, SEEK_SET);
    fwrite(&carpeta, sizeof (char), 1, disco);
    fseek(disco, sb.s_inode_start, SEEK_SET);
    fwrite(&ino_nuevo, sizeof (inodo), 1, disco);
    fseek(disco, sb.s_block_start, SEEK_SET);
    fwrite(&raiz, sizeof (bloque_carpeta), 1, disco);

    fclose(disco);
    printf("CREACION DE CARPETA RAIZ COMPLETA\n.");
    printf("Creando archivo users.txt.\n");
    SB super;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("\n");
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&super, sizeof (SB), 1, disco);
    char contenido_users[] = "1,g,root\n1,u,root,root,201403775\n";

    inodo inodo_nuevo2;

    inodo_nuevo2.I_gid = 1;
    inodo_nuevo2.i_atime = time(NULL);
    for (int i = 0; i < 15; i++)
        inodo_nuevo2.i_block[i] = -1;
    inodo_nuevo2.i_block[0] = super.s_first_blo;
    inodo_nuevo2.i_ctime = time(NULL);
    inodo_nuevo2.i_mtime = time(NULL);
    inodo_nuevo2.i_nlink = 0;
    strcpy(inodo_nuevo2.i_pathlink, "/users.txt");
    inodo_nuevo2.i_perm = 777;
    inodo_nuevo2.i_size = strlen(contenido_users);
    inodo_nuevo2.i_type = '0';
    inodo_nuevo2.i_uid = 1;


    super.s_first_blo++;
    super.s_first_ino++;
    super.s_free_blocks_count--;
    super.s_free_inodes_count--;

    bloque_archivo nuevo_bloque;

    for (int i = 0; contenido_users[i]; i++) {
        nuevo_bloque.b_content[i] = contenido_users[i];
    }

    bloque_carpeta padre;

    fseek(disco, super.s_block_start, SEEK_SET);
    fread(&padre, sizeof (bloque_carpeta), 1, disco);

    padre.b_content[2].b_inodo = 1;
    strcpy(padre.b_content[2].bname, "users.txt");

    fseek(disco, super.s_block_start, SEEK_SET);
    fwrite(&padre, sizeof (bloque_carpeta), 1, disco);
    fseek(disco, super.s_block_start + sizeof (bloque_carpeta), SEEK_SET);
    fwrite(&nuevo_bloque, sizeof (bloque_archivo), 1, disco);

    fseek(disco, super.s_inode_start + sizeof (inodo), SEEK_SET);
    fwrite(&inodo_nuevo2, sizeof (inodo), 1, disco);

    fseek(disco, super.s_bm_inode_start, SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);
    fwrite(&lleno, sizeof (char), 1, disco);

    fseek(disco, super.s_bm_block_start, SEEK_SET);
    fwrite(&carpeta, sizeof (char), 1, disco);
    fwrite(&archivo, sizeof (char), 1, disco);

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&super, sizeof (SB), 1, disco);

    fclose(disco);

    printf("ARCHIVO USERS.TXT CREADO\n");
    printf("------------Generacion Completa------------\n");

}

void add_formato(Montaje *montaje, char *id, int add, int unit) {
    printf("llego\n");
}
