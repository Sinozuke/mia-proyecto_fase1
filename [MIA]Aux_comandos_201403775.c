#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "[MIA]Aux_comandos_201403775.h"

int isnumero(char *token) {

    for (int i = 0; token[i] != '\0'; i++) {
        if (isalpha(token[i])) {
            return 0;
        }
    }

    return 1;
}

char Fast_Full(char *valor) {

    char cadena[20];

    strcpy(cadena, valor);

    for (int i = 0; cadena[i]; i++)
        cadena[i] = tolower(cadena[i]);

    if (strlen(valor) == 4) {
        if (strstr(cadena, "fast")) {
            return 'a';
        }
        if (strstr(cadena, "full")) {
            return 'u';
        }
    }
    return 'n';

}

char BF_FF_WF(char *valor) {
    if (strlen(valor) == 2) {
        char *pos = strstr(valor, "bf");
        if (pos) {
            return 'b';
        }
        pos = strstr(valor, "ff");
        if (pos) {
            return 'f';
        }
        pos = strstr(valor, "wf");
        if (pos) {
            return 'w';
        }
    }
    return 'n';
}

int k_m(char *valor) {
    if (strlen(valor) == 1) {
        if (strstr(valor, "m") || strstr(valor, "M")) {
            return 1;
        }
        if (strstr(valor, "k") || strstr(valor, "K")) {
            return 2;
        }
        if (strstr(valor, "b") || strstr(valor, "B")) {
            return 3;
        }
    }
    return 0;
}

int name_rep(char *token) {

    char cadena[10];

    strcpy(cadena, token);

    for (int i = 0; cadena[i]; i++)
        cadena[i] = tolower(cadena[i]); //<--convierte a minusculas

    if (strcmp(cadena, "mbr") == 0) {
        return 1;
    }
    if (strcmp(cadena, "disk") == 0) {
        return 2;
    }
    if (strcmp(cadena, "inode") == 0) {
        return 3;
    }
    if (strcmp(cadena, "journaling") == 0) {
        return 4;
    }
    if (strcmp(cadena, "block") == 0) {
        return 5;
    }
    if (strcmp(cadena, "bm_inode") == 0) {
        return 6;
    }
    if (strcmp(cadena, "bm_block") == 0) {
        return 7;
    }
    if (strcmp(cadena, "tree") == 0) {
        return 8;
    }
    if (strcmp(cadena, "sb") == 0) {
        return 9;
    }
    if (strcmp(cadena, "file") == 0) {
        return 10;
    }
    if (strcmp(cadena, "ls+i") == 0) {
        return 11;
    }
    if (strcmp(cadena, "ls+l") == 0) {
        return 12;
    }
    return 0;
}

int ext2_ext3(char *token) {

    char cadena[20];

    strcpy(cadena, token);

    for (int i = 0; cadena[i]; i++)
        cadena[i] = tolower(cadena[i]);

    if (strstr(cadena, "2fs"))
        return 2;
    if (strstr(cadena, "3fs"))
        return 3;

    return -1;

}

int parametros(char *token) {

    char cadena[100];

    strcpy(cadena, token);

    for (int i = 0; cadena[i]; i++)
        cadena[i] = tolower(cadena[i]);

    if (strstr(cadena, "&size"))
        return 1;
    if (strstr(cadena, "%unit"))
        return 2;
    if (strstr(cadena, "&path"))
        return 3;
    if (strstr(cadena, "&name"))
        return 4;
    if (strstr(cadena, "%type"))
        return 5;
    if (strstr(cadena, "%fit"))
        return 6;
    if (strstr(cadena, "%delete"))
        return 7;
    if (strstr(cadena, "%add"))
        return 8;
    if (strstr(cadena, "&id"))
        return 9;
    if (strstr(cadena, "%ruta"))
        return 10;
    if (strstr(cadena, "%fs"))
        return 11;
    if (strstr(cadena, "%p"))
        return 12;
    if (strstr(cadena, "&usr"))
        return 13;
    if (strstr(cadena, "&pwd"))
        return 14;
    if (strstr(cadena, "&grp"))
        return 15;
    return 0;
}

char *ncomillas(char *a) {
    char *r = malloc(sizeof (char)*strlen(a));
    int i;
    for (i = 0; a[i + 1] != '\"'; i++) {
        if (a[i + 1] != '\"') {
            r[i] = a[i + 1];
        }
    }
    r[i] = '\0';
    return r;
}

char *valor_real(int a, char *b) {
    char *r = malloc(sizeof (char)*(a + 1));
    int i;
    for (i = 0; i < a; i++) {
        r[i] = b[i];
    }
    r[i] = '\0';
    return r;
}

int longitud_real(char *a) {
    int i = 1;
    while (a[i - 1] != '\0')
        i++;
    return i - 1;
}

int pasa(char *token, int opcion1, int opcion2) {
    int dos_puntos = 0;
    int comillas = 0;
    int i;
    for (i = 0; token[i]; i++)
        if (token[i] == '-' && token[i + 1] == '>') {
            dos_puntos++;
            dos_puntos++;
        } else if (token[i] == '\"')
            comillas++;

    if (opcion1)
        if (dos_puntos == 2)
            return 1;
        else
            return 0;
    else
        if (comillas == 2)
        return 1;
    else
        return 0;
}

int pasa2(char *token) {
    int i;
    for (i = 0; token[i]; i++)
        if (token[i] == '-' && token[i + 1] == '>')
            return i + 2;
    return -1;
}

int opcion(char *token) {

    char cadena[100];

    strcpy(cadena, token);

    for (int i = 0; cadena[i]; i++)
        cadena[i] = tolower(cadena[i]);

    if (strcmp(cadena, "mkdisk") == 0)
        return 1;
    if (strcmp(cadena, "rmdisk") == 0)
        return 2;
    if (strcmp(cadena, "fdisk") == 0)
        return 3;
    if (strcmp(cadena, "mount") == 0)
        return 4;
    if (strcmp(cadena, "umount") == 0)
        return 5;
    if (strcmp(cadena, "rep") == 0)
        return 6;
    if (strcmp(cadena, "exec") == 0)
        return 7;
    if (strcmp(cadena, "exit") == 0)
        return 8;
    if (strcmp(cadena, "df") == 0)
        return 9;
    if (strcmp(cadena, "du") == 0)
        return 10;
    if (strcmp(cadena, "mkfs") == 0)
        return 11;
    if (strcmp(cadena, "login") == 0)
        return 12;
    if (strcmp(cadena, "logout") == 0)
        return 13;
    if (strcmp(cadena, "mkgrp") == 0)
        return 14;
    if (strcmp(cadena, "rmgrp") == 0)
        return 15;
    if (strcmp(cadena, "mkusr") == 0)
        return 16;
    if (strcmp(cadena, "rmusr") == 0)
        return 17;
    if (strcmp(cadena, "chmod") == 0)
        return 18;
    if (strcmp(cadena, "mkfile") == 0)
        return 19;
    if (strcmp(cadena, "cat") == 0)
        return 20;
    if (strcmp(cadena, "rm") == 0)
        return 21;
    if (strcmp(cadena, "edit") == 0)
        return 22;
    if (strcmp(cadena, "ren") == 0)
        return 23;
    if (strcmp(cadena, "mkdir") == 0)
        return 24;
    if (strcmp(cadena, "cp") == 0)
        return 25;
    if (strcmp(cadena, "mv") == 0)
        return 26;
    if (strcmp(cadena, "find") == 0)
        return 27;
    if (strcmp(cadena, "chown") == 0)
        return 28;
    if (strcmp(cadena, "chgrp") == 0)
        return 29;
    if (strcmp(cadena, "ln") == 0)
        return 30;
    if (strcmp(cadena, "unlink") == 0)
        return 31;
    if (strcmp(cadena, "tune2fs") == 0)
        return 32;
    if (strcmp(cadena, "recovery") == 0)
        return 33;
    if (strcmp(cadena, "loss") == 0)
        return 34;
    return 0;
}

