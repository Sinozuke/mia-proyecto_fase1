#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_inode(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_dot[100] = "dot -Tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, directorio);
    strcat(comando_dot, ".jpg INODE.dot");

    strcat(comando_abertura, directorio);
    strcat(comando_abertura, ".jpg");


    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;

    reporte = fopen("INODE.dot", "w");

    int contador = -1;

    SB sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "rankdir=LR;\n");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    char bloque = 'v';

    fseek(disco, sb.s_bm_inode_start, SEEK_SET);

    inodo ino;

    for (int i = 0; i < sb.s_inodes_count; i++) {
        fread(&bloque, sizeof (char), 1, disco);
        if (bloque == '1') {
            fseek(disco, sb.s_inode_start + i * sizeof (inodo), SEEK_SET);
            fread(&ino, sizeof (inodo), 1, disco);
            fprintf(reporte, "\t\"Inode%i\" [label=<\n", i);
            fprintf(reporte, "<TABLE>\n");
            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD COLSPAN=\"2\">Inodo %i</TD>\n", i + 1);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_nlink</TD>\n");
            fprintf(reporte, "<TD>%i</TD>\n", ino.i_nlink);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_pathlink</TD>\n");
            fprintf(reporte, "<TD>%s</TD>\n", ino.i_pathlink);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_uid</TD>\n");
            fprintf(reporte, "<TD>%i</TD>\n", ino.i_uid);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_gid</TD>\n");
            fprintf(reporte, "<TD>%i</TD>\n", ino.I_gid);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_size</TD>\n");
            fprintf(reporte, "<TD>%i</TD>\n", ino.i_size);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_atime</TD>\n");
            fprintf(reporte, "<TD>%s</TD>\n", ctime(&ino.i_atime));
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_ctime</TD>\n");
            fprintf(reporte, "<TD>%s</TD>\n", ctime(&ino.i_ctime));
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_mtime</TD>\n");
            fprintf(reporte, "<TD>%s</TD>\n", ctime(&ino.i_mtime));
            fprintf(reporte, "</TR>\n");

            for (int j = 0; j < 15; j++) {
                fprintf(reporte, "<TR>\n");
                fprintf(reporte, "<TD>i_block_%i</TD>\n", j + 1);
                fprintf(reporte, "<TD>%i</TD>\n", ino.i_block[j]);
                fprintf(reporte, "</TR>\n");
            }

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_type</TD>\n");
            fprintf(reporte, "<TD>%c</TD>\n", ino.i_type);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "<TR>\n");
            fprintf(reporte, "<TD>i_perm</TD>\n");
            fprintf(reporte, "<TD>%i</TD>\n", ino.i_perm);
            fprintf(reporte, "</TR>\n");

            fprintf(reporte, "</TABLE>>];");
            fseek(disco, sb.s_bm_inode_start + i * sizeof (char) + sizeof (char), SEEK_SET);
            if (contador != -1) {
                fprintf(reporte, "\"Inode%i\"->\"Inode%i\";\n", contador, i);
            }
            contador = i;
        }
    }

    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}