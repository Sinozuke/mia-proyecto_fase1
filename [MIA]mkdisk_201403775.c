#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]Datos_201403775.h"
#include "[MIA]mkdisk_201403775.h"

int crear_directorios(char *path);

void generar_disco(char *nombre, char *path, int tamano, int tipo) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    if (tipo == 1) {
        if (tamano < 10) {
            printf("ERROR: le valor minimo para un nuevo Disco es de 10 Mb.\n");
            printf("**********GENERACION FALLIDA***********\n\n");
            return;
        }
    } else {
        if (tamano < 10240) {
            printf("ERROR: el valor minimo para un nuevo Disco Duro es de 10240Kb.\n");
            printf("**********GENERACION FALLIDA***********\n\n");
            return;
        }
    }

    char Byte = '\0';

    FILE *disco;

    char *nombred = directorio;
    strcat(nombred, nombre);
    if (strstr(nombred, ".dsk") == 0)
        strcat(nombred, ".dsk");
    disco = fopen(nombred, "wb");

    if (!disco) {
        printf("ERROR: el Disco no ha podido crearse.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    if (tipo == 1)
        for (int j = 0; j < tamano; j++)
            for (int i = 0; i < 1024; i++)
                fwrite(&Byte, sizeof (char), 1024, disco);
    else
        for (int i = 0; i < tamano; i++)
            fwrite(&Byte, sizeof (char), 1024, disco);

    srand((unsigned int) time(NULL));

    MBR nuevo_mbr;
    nuevo_mbr.mbr_fecha_creacion = time(NULL);

    if (tipo == 1)
        nuevo_mbr.mbr_tamano = 1024 * 1024 * tamano;
    else
        nuevo_mbr.mbr_tamano = 1024 * tamano;

    particion particion_vacia;

    particion_vacia.part_status = 'n';
    particion_vacia.part_type = 'n';
    particion_vacia.part_fit = 'n';
    particion_vacia.part_start = -1;
    particion_vacia.part_size = 0;
    strcpy(particion_vacia.part_name, "-vacia-");

    nuevo_mbr.mbr_partition_1 = particion_vacia;
    nuevo_mbr.mbr_partition_2 = particion_vacia;
    nuevo_mbr.mbr_partition_3 = particion_vacia;
    nuevo_mbr.mbr_partition_4 = particion_vacia;

    nuevo_mbr.mbr_disk_signature = rand();

    fseek(disco, 0, SEEK_SET);

    fwrite(&nuevo_mbr, sizeof (MBR), 1, disco);

    fclose(disco);
    printf("************GENERACION COMPLETA************\n\n");
}

int crear_directorios(char *path) {

    char *direc = strtok(path, "/");

    if (!direc)
        return 0;

    char dirreccion[200] = "/";

    struct stat st = {0};


    while (direc) {
        if (!strstr(dirreccion, ".txt") && !strstr(dirreccion, ".jpg"))
            if (stat(dirreccion, &st) == -1)
                mkdir(dirreccion, 0700);

        strcat(dirreccion, direc);
        strcat(dirreccion, "/");
        direc = strtok(NULL, "/");
    }
    return 1;
}
