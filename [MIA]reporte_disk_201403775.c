#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_disk(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    char comando_dot[100] = "dot -Tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, directorio);
    strcat(comando_dot, ".jpg DISK.dot");

    strcat(comando_abertura, directorio);
    strcat(comando_abertura, ".jpg");

    FILE *disco;

    disco = fopen(path_disco, "r+b");

    MBR mbr;
    EBR ebr;

    fseek(disco, 0, SEEK_SET);
    fread(&mbr, sizeof (mbr), 1, disco);

    FILE *reporte;

    reporte = fopen("DISK.dot", "w");
    int extendida = 0;
    int num_ebrs = 0;
    char part_extendida[500] = "<TR>\n";

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "\t\"mbr\" [label=<\n");
    fprintf(reporte, "<TABLE BORDER=\"1\" CELLBORDER=\"1\" CELLSPACING=\"1\" CELLPADDING=\"3\" COLOR=\"BLUE\">\n");

    for (int i = 0; i < 4; i++) {
        switch (i) {
            case 0:
                if (mbr.mbr_partition_1.part_type == 'e')
                    extendida = 1;
                break;
            case 1:
                if (mbr.mbr_partition_2.part_type == 'e')
                    extendida = 2;
                break;
            case 2:
                if (mbr.mbr_partition_3.part_type == 'e')
                    extendida = 3;
                break;
            case 3:
                if (mbr.mbr_partition_4.part_type == 'e')
                    extendida = 4;
                break;
        }
    }

    if (extendida == 0) {
        fprintf(reporte, "<TR>");
        fprintf(reporte, "\t<TD>MBR</TD>");

        if (mbr.mbr_partition_1.part_type == 'p')
            fprintf(reporte, "\t<TD>Primaria</TD>");
        else
            fprintf(reporte, "\t<TD>Espacio Vacio</TD>");

        if (mbr.mbr_partition_2.part_type == 'p')
            fprintf(reporte, "\t<TD>Primaria</TD>");
        else
            fprintf(reporte, "\t<TD>Espacio Vacio</TD>");

        if (mbr.mbr_partition_3.part_type == 'p')
            fprintf(reporte, "\t<TD>Primaria</TD>");
        else
            fprintf(reporte, "\t<TD>Espacio Vacio</TD>");

        if (mbr.mbr_partition_4.part_type == 'p')
            fprintf(reporte, "\t<TD>Primaria</TD>");
        else
            fprintf(reporte, "\t<TD>Espacio Vacio</TD>");

        fprintf(reporte, "</TR>");
    } else {

        switch (extendida) {
            case 1:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 2:
                fseek(disco, mbr.mbr_partition_2.part_start, SEEK_SET);
                break;
            case 3:
                fseek(disco, mbr.mbr_partition_3.part_start, SEEK_SET);
                break;
            case 4:
                fseek(disco, mbr.mbr_partition_4.part_start, SEEK_SET);
                break;
        }

        fread(&ebr, sizeof (EBR), 1, disco);
        strcat(part_extendida, "<TD>EBR</TD>\n");
        num_ebrs++;

        while (ebr.part_next != -1) {
            if (ebr.part_status != 'n') {
                strcat(part_extendida, "<TD>Logica</TD>\n");
                num_ebrs++;
            }
            if ((ebr.part_size + ebr.part_start) < ebr.part_next) {
                strcat(part_extendida, "<TD>Vacio</TD>\n");
                num_ebrs++;
            }
        }

        strcat(part_extendida, "<TD>Vacio</TD>\n");
        num_ebrs++;

        strcat(part_extendida, "</TR>\n");

        fprintf(reporte, "<TR>");
        fprintf(reporte, "\t<TD ROWSPAN=\"2\">MBR</TD>");

        if (mbr.mbr_partition_1.part_type == 'p')
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Primaria</TD>");
        else if (mbr.mbr_partition_1.part_type == 'e')
            fprintf(reporte, "\t<TD COLSPAN=\"%i\">Extendida</TD>", num_ebrs);
        else
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Espacio Vacio</TD>");

        if (mbr.mbr_partition_2.part_type == 'p')
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Primaria</TD>");
        else if (mbr.mbr_partition_2.part_type == 'e')
            fprintf(reporte, "\t<TD COLSPAN=\"%i\">Extendida</TD>", num_ebrs);
        else
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Espacio Vacio</TD>");

        if (mbr.mbr_partition_3.part_type == 'p')
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Primaria</TD>");
        else if (mbr.mbr_partition_3.part_type == 'e')
            fprintf(reporte, "\t<TD COLSPAN=\"%i\">Extendida</TD>", num_ebrs);
        else
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Espacio Vacio</TD>");

        if (mbr.mbr_partition_4.part_type == 'p')
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Primaria</TD>");
        else if (mbr.mbr_partition_4.part_type == 'e')
            fprintf(reporte, "\t<TD COLSPAN=\"%i\">Extendida</TD>", num_ebrs);
        else
            fprintf(reporte, "\t<TD ROWSPAN=\"2\">Espacio Vacio</TD>");

        fprintf(reporte, "</TR>");

        fprintf(reporte, "%s", part_extendida);
    }
    fprintf(reporte, "</TABLE>>];");

    fprintf(reporte, "}");
    fclose(disco);
    fclose(reporte);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");

}
