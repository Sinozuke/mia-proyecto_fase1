#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]Login_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);
int primer_montaje(Montaje *montaje);
int primer_id(Montaje *montaje, int a);
void obtener_cont_archivo(char *contenido, inodo *ino, FILE *disco, SB *sb);
void obtener_cont_archivo_nivel_0(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera);
void obtener_cont_archivo_nivel_1(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera);
void obtener_cont_archivo_nivel_2(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera);
int contar_saltos(char *cont);
void almacenar_registros(Registro *regis, char *cont);
void vaciar(Registro *registro, int numero);

void login(Montaje *montaje, Sesion *sesion, char *username, char *password, char *id, int p_id) {

    char path_disco[100];
    ids encontrado;
    int a, b, c;

    if (p_id) {
        if (strlen(montaje[id[2] - 97].path) == 0) {
            printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
            printf("----------FORMATEO FALLIDO------------");
            return;
        }

        strcpy(path_disco, montaje[id[2] - 97].path);

        a = encontrar_id(montaje, id[2] - 97, id);

        if (a == -1) {
            printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
            printf("----------FORMATEO FALLIDO------------");
            return;
        }

        encontrado = montaje[id[2] - 97].lista[a];
        b = id[2] - 97;
        c = a;
    } else {
        b = primer_montaje(montaje);
        if (b == -1){
            printf("ERROR: No hay Particiones Montadas.\n");
            return;
        }
        strcpy(path_disco, montaje[b].path);
        c = primer_id(montaje, b);
        encontrado = montaje[b].lista[c];
    }

    if (encontrado.status != '2' && encontrado.status != '3') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB sb;
    inodo ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fread(&ino, sizeof (inodo), 1, disco);

    char contenido[ino.i_size];
    strcpy(contenido, "");
    obtener_cont_archivo(contenido, &ino, disco, &sb);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    fclose(disco);

    int bandera=1;

    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, username) == 0) {
                if (strcmp(registros[i].password, password) == 0) {
                    sesion->UID = registros[i].id;
                    for (int j = 0; j < numero_registros && bandera; j++)
                        if(registros[j].tipo=='g')
                            if (strcmp(registros[i].grupo, registros[j].grupo) == 0)
                                sesion->GID = registros[j].id;
                    sesion->lista = c;
                    sesion->montaje = b;
                    printf("--Logeado como \"%s\"--\n", username);
                    return;
                } else {
                    printf("--Contaseña Incorrecta--\n");
                    return;
                }
            }
        }
    }
    printf("--No Hay Ningun usuario como \"%s\"\n", username);
}

int primer_montaje(Montaje *montaje) {
    for (int i = 0; i < 26; i++) {
        if (strlen(montaje[i].path) != 0) {
            return i;
        }
    }
    return -1;
}

int primer_id(Montaje *montaje, int a) {
    for (int i = 0; i < 100; i++) {
        if (strlen(montaje[a].lista[i].nombre) != 0) {
            return i;
        }
    }
    return -1;
}

void obtener_cont_archivo(char *contenido, inodo *ino, FILE *disco, SB *sb) {
    int bandera = 0;
    bloque_archivo archivo;
    int lon = strlen(contenido);
    for(int i=0;i<lon;i++){
        contenido[i]='\0';
    }
    int escritos=0;
    

    for (int i = 0; i < 12 && !bandera; i++) {
        if (ino->i_block[i] != -1) {
            fseek(disco, sb->s_block_start + sizeof (bloque_archivo) * ino->i_block[i], SEEK_SET);
            fread(&archivo, sizeof (bloque_archivo), 1, disco);
            for(int i=0;i<64;i++){
                contenido[escritos]=archivo.b_content[i];
                escritos++;
            }
        } else {
            bandera = 1;
        }
    }

    if (bandera)
        return;

    bloque_apuntadores apunt;

    fseek(disco, sb->s_block_start + sizeof (bloque_archivo) * ino->i_block[12], SEEK_SET);
    fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
    obtener_cont_archivo_nivel_0(&apunt, contenido, disco, sb, &bandera);

    if (bandera)
        return;

    fseek(disco, sb->s_block_start + sizeof (bloque_archivo) * ino->i_block[13], SEEK_SET);
    fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
    obtener_cont_archivo_nivel_0(&apunt, contenido, disco, sb, &bandera);

    if (bandera)
        return;

    fseek(disco, sb->s_block_start + sizeof (bloque_archivo) * ino->i_block[14], SEEK_SET);
    fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
    obtener_cont_archivo_nivel_0(&apunt, contenido, disco, sb, &bandera);

}

void obtener_cont_archivo_nivel_0(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera) {
    bloque_archivo archivo;
    for (int i = 0; i < 15 && !(*bandera); i++) {
        if (ba->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + sizeof (bloque_archivo) * ba->b_pointers[i], SEEK_SET);
            fread(&archivo, sizeof (bloque_archivo), 1, disco);
            strcpy(contenido, archivo.b_content);
        } else {
            *bandera = 1;
        }
    }
}

void obtener_cont_archivo_nivel_1(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera) {
    bloque_apuntadores apunt;
    for (int i = 0; i < 15 && !(*bandera); i++) {
        if (ba->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + sizeof (bloque_apuntadores) * ba->b_pointers[i], SEEK_SET);
            fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
            obtener_cont_archivo_nivel_0(&apunt, contenido, disco, sb, bandera);
        } else {
            *bandera = 1;
        }
    }
}

void obtener_cont_archivo_nivel_2(bloque_apuntadores *ba, char *contenido, FILE *disco, SB *sb, int *bandera) {
    bloque_apuntadores apunt;
    for (int i = 0; i < 15 && !(*bandera); i++) {
        if (ba->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + sizeof (bloque_apuntadores) * ba->b_pointers[i], SEEK_SET);
            fread(&apunt, sizeof (bloque_apuntadores), 1, disco);
            obtener_cont_archivo_nivel_1(&apunt, contenido, disco, sb, bandera);
        } else {
            *bandera = 1;
        }
    }
}
