#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_sb(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_dot[100] = "dot -Tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, directorio);
    strcat(comando_dot, ".jpg SB.dot");

    strcat(comando_abertura, directorio);
    strcat(comando_abertura, ".jpg");


    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;

    reporte = fopen("SB.dot", "w");

    SB sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "\t\"SB\" [label=<\n");
    fprintf(reporte, "<TABLE>\n");

    fprintf(reporte, "<TR>");
    fprintf(reporte, "<TD COLSPAN=\"2\">SuperBloque %s en %s</TD>", encontrado.nombre, montaje[id[2] - 97].path);
    fprintf(reporte, "</TR>");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>Nombre</TD>\n");
    fprintf(reporte, "<TD>Valor</TD>\n");
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_inodes_count</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_inodes_count);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_blocks_count</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_blocks_count);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_free_blocks_count</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_free_blocks_count);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_free_inodes_count</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_free_inodes_count);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_mtime</TD>\n");
    fprintf(reporte, "<TD>%s</TD>\n", ctime(&sb.s_mtime));
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_umtime</TD>\n");
    fprintf(reporte, "<TD>%s</TD>\n", ctime(&sb.s_umtime));
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_mnt_count</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_mnt_count);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_magic</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_magic);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_inode_size</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_inode_size);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_block_size</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_block_size);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_first_ino</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_first_ino);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_first_blo</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_first_blo);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_bm_inode_start</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_bm_inode_start);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_bm_block_start</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_bm_block_start);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_inode_start</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_inode_start);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "<TR>\n");
    fprintf(reporte, "<TD>s_block_start</TD>\n");
    fprintf(reporte, "<TD>%i</TD>\n", sb.s_block_start);
    fprintf(reporte, "</TR>\n");

    fprintf(reporte, "</TABLE>>];");
    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}
