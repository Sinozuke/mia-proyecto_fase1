#include "[MIA]Datos_201403775.h"
#ifndef USERS_H
#define USERS_H

void crear_usuario(Montaje *montaje, Sesion *sesion, char *nombre,char *grupo,char *password, char *id);
void eliminar_usuario(Montaje *montaje, Sesion *sesion, char *nombre, char *id);

#endif /* USERS_H */

