#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "[MIA]Directorios_201403775.h"

void escribir_D(FILE *disco, SB *sb, Sesion *sesion, inodo *ino, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path);
void D_exist_n0(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera);
void D_exist_n1(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera);
void D_exist_n2(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera);
void crear_folder(FILE *disco, SB *sb, Sesion *sesion, int inodo_padre, carpetas *carpes, int carpe_actual, int *ino_insert);
void primer_b_f(FILE *disco, SB *sb);
void primer_i_f(FILE *disco, SB *sb);
int contar_carpes(char *path);
void vaciar_carpes(carpetas *carpes, int numero);
void llenar_carpes(carpetas *carpes, char *cont);

void crear_Directorio(Montaje *montaje, Sesion *sesion, int p, char *id, char *path, int permisos) {
    char path_1[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }
    strcpy(path_1, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    char carpeta_a_crear[strlen(path)];
    strncpy(carpeta_a_crear, path, sizeof (carpeta_a_crear));

    char carpeta_final[12];

    char *carpeta_p;
    carpeta_p = strtok(path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > 12) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            return;
        }
        strcpy(carpeta_final, carpeta_p);
        carpeta_p = strtok(NULL, "/");
    }


    inodo leido;
    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);


    fseek(disco, sb.s_inode_start, SEEK_SET);
    fread(&leido, sizeof (inodo), 1, disco);

    escribir_D(disco, &sb, sesion, &leido, carpes, 0, 0, 0, carpeta_final, p, carpeta_a_crear);

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&sb, sizeof (SB), 1, disco);

    fclose(disco);

}

void llenar_carpes(carpetas *carpes, char *cont) {
    int contador = 0;
    int contador2 = 0;
    int lon = strlen(cont);
    for (int i = 1; i < lon; i++) {
        if (cont[i] != '/') {
            carpes[contador].bname[contador2] = cont[i];
            contador2++;
        } else {
            contador++;
            contador2 = 0;
        }
    }
}

void vaciar_carpes(carpetas *carpes, int numero) {
    for (int i = 0; i < numero; i++)
        for (int j = 0; j < 13; j++)
            carpes[i].bname[j] = '\0';
}

int contar_carpes(char *path) {
    int devolver = 0;
    for (int i = 0; path[i]; i++)
        if (path[i] == '/')
            devolver++;
    return devolver;
}

void crear_folder(FILE *disco, SB *sb, Sesion *sesion, int inodo_padre, carpetas *carpes, int carpe_actual, int *ino_insert) {
    bloque_carpeta carpeta;
    inodo ino;
    char car = 'd';
    char lleno = '1';

    ino.I_gid = sesion->GID;
    ino.i_atime = time(NULL);
    for (int i = 0; i < 15; i++) {
        ino.i_block[i] = -1;
    }
    ino.i_ctime = time(NULL);
    ino.i_mtime = time(NULL);
    ino.i_nlink = 0;
    strncpy(ino.i_pathlink, carpes[carpe_actual].bname, 12 * sizeof (char));
    ino.i_perm = 664;
    ino.i_size = 0;
    ino.i_type='1';
    ino.i_uid = sesion->UID;

    ino.i_block[0] = sb->s_first_blo;

    sb->s_free_blocks_count--;

    strncpy(carpeta.b_content[0].bname, ".", sizeof (char));
    strncpy(carpeta.b_content[1].bname, "..", 2 * sizeof (char));
    carpeta.b_content[0].b_inodo = sb->s_first_ino;
    carpeta.b_content[1].b_inodo = inodo_padre;

    strncpy(carpeta.b_content[2].bname, "\0", sizeof (char));
    strncpy(carpeta.b_content[3].bname, "\0", sizeof (char));
    carpeta.b_content[2].b_inodo = -1;
    carpeta.b_content[3].b_inodo = -1;


    sb->s_free_inodes_count--;

    *ino_insert = sb->s_first_ino;

    fseek(disco, sb->s_inode_start + sb->s_first_ino * sizeof (inodo), SEEK_SET);
    fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, sb->s_block_start + sb->s_first_blo * sizeof (bloque_carpeta), SEEK_SET);
    fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);
    fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
    fwrite(&car, sizeof (char), 1, disco);
    fseek(disco, sb->s_bm_inode_start + sb->s_first_ino * sizeof (char), SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);

    primer_i_f(disco, sb);
    primer_b_f(disco, sb);

    printf("Carpeta Creada \"%s\"\n", carpes[carpe_actual].bname);

}

void escribir_D(FILE *disco, SB *sb, Sesion *sesion, inodo *ino, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path) {

    bloque_carpeta carpeta;
    inodo siguiente;
    char carpe = 'd';
    char apuntad = 'a';

    for (int i = 0; i < 12; i++) {
        if (ino->i_block[i] != -1) {
            fseek(disco, sb->s_block_start + ino->i_block[i] * sizeof (bloque_carpeta), SEEK_SET);
            fread(&carpeta, sizeof (bloque_carpeta), 1, disco);
            for (int j = 0; j < 4; j++) {
                if (carpeta.b_content[j].b_inodo != -1) {
                    if (strcmp(carpeta.b_content[j].bname, ".") != 0 && strcmp(carpeta.b_content[j].bname, "..") != 0) {
                        if (strcmp(carpeta.b_content[j].bname, carpes[carpe_actual].bname) == 0) {
                            if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                                printf("ERROR: La Carpeta \"%s\" ya esta creada.\n", nombre);
                                return;
                            } else {
                                fseek(disco, sb->s_inode_start + carpeta.b_content[j].b_inodo * sizeof (inodo), SEEK_SET);
                                fread(&siguiente, sizeof (inodo), 1, disco);

                                escribir_D(disco, sb, sesion, &siguiente, carpes, carpe_actual + 1, ino_actual, carpeta.b_content[j].b_inodo, nombre, p, path);
                                fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
                                time(&ino->i_atime);
                                fwrite(ino, sizeof (inodo), 1, disco);
                                return;
                            }
                        }
                    }
                } else {
                    if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                        int ino_insert;
                        crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);
                        carpeta.b_content[j].b_inodo = ino_insert;
                        strncpy(carpeta.b_content[j].bname, nombre, 12 * sizeof (char));
                        fseek(disco, sb->s_block_start + ino->i_block[i] * sizeof (bloque_carpeta), SEEK_SET);
                        fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);
                        return;
                    } else {
                        if (p) {
                            int ino_insert;
                            inodo aux;
                            crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);

                            fseek(disco, sb->s_inode_start + ino_insert * sizeof (inodo), SEEK_SET);
                            fread(&aux, sizeof (inodo), 1, disco);


                            escribir_D(disco, sb, sesion, &aux, carpes, carpe_actual + 1, ino_actual, ino_insert, nombre, p, path);


                            strncpy(carpeta.b_content[j].bname, carpes[carpe_actual].bname, sizeof (carpeta.b_content[j].bname));
                            carpeta.b_content[j].b_inodo = ino_insert;
                            fseek(disco, sb->s_block_start + ino->i_block[i] * sizeof (bloque_carpeta), SEEK_SET);
                            fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);
                            fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
                            time(&ino->i_atime);
                            fwrite(ino, sizeof (inodo), 1, disco);
                            return;
                        } else {
                            printf("ERROR: la carpeta \"%s\" No Exite.\n", carpes[carpe_actual].bname);
                            fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
                            time(&(*ino).i_atime);
                            fwrite(ino, sizeof (inodo), 1, disco);
                            return;
                        }
                    }
                }
            }
        } else {
            if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                bloque_carpeta carpeta1;

                carpeta1.b_content[0].b_inodo = -1;
                carpeta1.b_content[1].b_inodo = -1;
                carpeta1.b_content[2].b_inodo = -1;
                carpeta1.b_content[3].b_inodo = -1;
                strncpy(carpeta1.b_content[0].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[1].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[2].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[3].bname, "\0", sizeof (char));

                int ino_insert;
                int inserto = sb->s_first_blo;
                sb->s_free_blocks_count--;
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&carpe, sizeof (char), 1, disco);
                primer_b_f(disco, sb);
                crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);


                carpeta1.b_content[0].b_inodo = ino_insert;
                strncpy(carpeta1.b_content[0].bname, nombre, sizeof (carpeta1.b_content[0].bname));

                fseek(disco, sb->s_block_start + inserto * sizeof (bloque_carpeta), SEEK_SET);
                fwrite(&carpeta1, sizeof (bloque_carpeta), 1, disco);

                ino->i_block[i] = inserto;
                time(&ino->i_mtime);

                fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
                fwrite(ino, sizeof (inodo), 1, disco);

                return;
            } else {
                if (p) {
                    int ino_insert;
                    int inserto = sb->s_first_blo;
                    sb->s_free_blocks_count--;
                    fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                    fwrite(&carpe, sizeof (char), 1, disco);
                    primer_b_f(disco, sb);
                    crear_folder(disco, sb, sesion, ino_padre, carpes, carpe_actual, &ino_insert);
                    carpeta.b_content[0].b_inodo = ino_insert;
                    strcpy(carpeta.b_content[0].bname, carpes[carpe_actual].bname);
                    fseek(disco, sb->s_block_start + inserto * sizeof (bloque_carpeta), SEEK_SET);
                    fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);

                    inodo aux;

                    fseek(disco, sb->s_inode_start + ino_insert * sizeof (inodo), SEEK_SET);
                    fread(&aux, sizeof (inodo), 1, disco);

                    escribir_D(disco, sb, sesion, &aux, carpes, carpe_actual + 1, ino_actual, ino_insert, nombre, p, path);

                    ino->i_block[i] = inserto;
                    time(&ino->i_mtime);

                    fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
                    fwrite(ino, sizeof (inodo), 1, disco);

                    return;
                } else {
                    printf("ERROR: la carpeta \"%s\" No Exite.\n", carpes[carpe_actual].bname);
                    return;
                }
            }
        }
    }

    int bandera = 1;

    bloque_apuntadores apunta;
    if (ino->i_block[12] != -1) {
        fseek(disco, sb->s_block_start + ino->i_block[12] * sizeof (bloque_apuntadores), SEEK_SET);
        fread(&apunta, sizeof (bloque_apuntadores), 1, disco);
        D_exist_n0(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
    } else {
        for (int j = 0; j < 16; j++)
            apunta.b_pointers[j] = -1;
        ino->i_block[12] = sb->s_first_blo;
        fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
        fwrite(&apuntad, sizeof (char), 1, disco);
        primer_b_f(disco, sb);
        D_exist_n0(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
        fseek(disco, sb->s_block_start + ino->i_block[12] * sizeof (bloque_apuntadores), SEEK_SET);
        fwrite(&apunta, sizeof (bloque_apuntadores), 1, disco);
    }

    if (bandera) {
        if (ino->i_block[13] != -1) {
            fseek(disco, sb->s_block_start + ino->i_block[13] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apunta, sizeof (bloque_apuntadores), 1, disco);
            D_exist_n1(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
        } else {
            for (int j = 0; j < 16; j++)
                apunta.b_pointers[j] = -1;
            ino->i_block[13] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apuntad, sizeof (char), 1, disco);
            primer_b_f(disco, sb);
            D_exist_n1(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
            fseek(disco, sb->s_block_start + ino->i_block[13] * sizeof (bloque_apuntadores), SEEK_SET);
            fwrite(&apunta, sizeof (bloque_apuntadores), 1, disco);
        }
    }

    if (bandera) {
        if (ino->i_block[14] != -1) {
            fseek(disco, sb->s_block_start + ino->i_block[14] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apunta, sizeof (bloque_apuntadores), 1, disco);
            D_exist_n2(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
        } else {
            for (int j = 0; j < 16; j++)
                apunta.b_pointers[j] = -1;
            ino->i_block[14] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apuntad, sizeof (char), 1, disco);
            primer_b_f(disco, sb);
            D_exist_n2(disco, sb, &apunta, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, &bandera);
            fseek(disco, sb->s_block_start + ino->i_block[14] * sizeof (bloque_apuntadores), SEEK_SET);
            fwrite(&apunta, sizeof (bloque_apuntadores), 1, disco);
        }
    }

    time(&ino->i_mtime);

    fseek(disco, sb->s_inode_start + ino_actual * sizeof (inodo), SEEK_SET);
    fwrite(ino, sizeof (inodo), 1, disco);

}

void D_exist_n0(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera) {
    bloque_carpeta carpeta;
    inodo siguiente;
    char carpe = 'd';
    for (int i = 0; i < 16; i++) {
        if (apunta->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_carpeta), SEEK_SET);
            fread(&carpeta, sizeof (bloque_carpeta), 1, disco);
            for (int j = 0; j < 4; j++) {
                if (carpeta.b_content[j].b_inodo != -1) {
                    if (strcmp(carpeta.b_content[j].bname, ".") != 0 && strcmp(carpeta.b_content[j].bname, "..") != 0) {
                        if (strcmp(carpeta.b_content[j].bname, carpes[carpe_actual].bname) == 0) {
                            if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                                printf("ERROR: La Carpeta \"%s\" ya esta creada.\n", nombre);
                                *bandera = 0;
                                return;
                            } else {
                                fseek(disco, sb->s_inode_start + carpeta.b_content[j].b_inodo * sizeof (inodo), SEEK_SET);
                                fread(&siguiente, sizeof (inodo), 1, disco);

                                escribir_D(disco, sb, sesion, &siguiente, carpes, carpe_actual + 1, ino_actual, carpeta.b_content[j].b_inodo, nombre, p, path);
                                *bandera = 0;
                                return;
                            }
                        }
                    }
                } else {
                    if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                        int ino_insert;
                        crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);
                        carpeta.b_content[j].b_inodo = ino_insert;
                        strncpy(carpeta.b_content[j].bname, nombre, 12 * sizeof (char));
                        fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_carpeta), SEEK_SET);
                        fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);
                        *bandera = 0;
                        return;
                    } else {
                        if (p) {
                            int ino_insert;
                            inodo aux;
                            crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);

                            fseek(disco, sb->s_inode_start + ino_insert * sizeof (inodo), SEEK_SET);
                            fread(&aux, sizeof (inodo), 1, disco);


                            escribir_D(disco, sb, sesion, &aux, carpes, carpe_actual + 1, ino_actual, ino_insert, nombre, p, path);


                            strncpy(carpeta.b_content[j].bname, carpes[carpe_actual].bname, sizeof (carpeta.b_content[j].bname));
                            carpeta.b_content[j].b_inodo = ino_insert;
                            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_carpeta), SEEK_SET);
                            fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);
                            return;
                        } else {
                            printf("ERROR: la carpeta \"%s\" No Exite.\n", carpes[carpe_actual].bname);
                            *bandera = 0;
                            return;
                        }
                    }
                }
            }
        } else {
            if (strcmp(carpes[carpe_actual].bname, nombre) == 0) {
                bloque_carpeta carpeta1;

                carpeta1.b_content[0].b_inodo = -1;
                carpeta1.b_content[1].b_inodo = -1;
                carpeta1.b_content[2].b_inodo = -1;
                carpeta1.b_content[3].b_inodo = -1;
                strncpy(carpeta1.b_content[0].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[1].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[2].bname, "\0", sizeof (char));
                strncpy(carpeta1.b_content[3].bname, "\0", sizeof (char));

                int ino_insert;
                int inserto = sb->s_first_blo;
                sb->s_free_blocks_count--;
                fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                fwrite(&carpe, sizeof (char), 1, disco);
                primer_b_f(disco, sb);
                crear_folder(disco, sb, sesion, ino_actual, carpes, carpe_actual, &ino_insert);


                carpeta1.b_content[0].b_inodo = ino_insert;
                strncpy(carpeta1.b_content[0].bname, nombre, sizeof (carpeta1.b_content[0].bname));

                fseek(disco, sb->s_block_start + inserto * sizeof (bloque_carpeta), SEEK_SET);
                fwrite(&carpeta1, sizeof (bloque_carpeta), 1, disco);

                apunta->b_pointers[i] = inserto;
                *bandera = 0;
                return;
            } else {
                if (p) {
                    int ino_insert;
                    int inserto = sb->s_first_blo;
                    sb->s_free_blocks_count--;
                    fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
                    fwrite(&carpe, sizeof (char), 1, disco);
                    primer_b_f(disco, sb);
                    crear_folder(disco, sb, sesion, ino_padre, carpes, carpe_actual, &ino_insert);
                    carpeta.b_content[0].b_inodo = ino_insert;
                    strncpy(carpeta.b_content[0].bname, carpes[carpe_actual].bname, 12 * sizeof (char));
                    fseek(disco, sb->s_block_start + inserto * sizeof (bloque_carpeta), SEEK_SET);
                    fwrite(&carpeta, sizeof (bloque_carpeta), 1, disco);

                    inodo aux;

                    fseek(disco, sb->s_inode_start + ino_insert * sizeof (inodo), SEEK_SET);
                    fread(&aux, sizeof (inodo), 1, disco);

                    escribir_D(disco, sb, sesion, &aux, carpes, carpe_actual + 1, ino_actual, ino_insert, nombre, p, path);

                    apunta->b_pointers[i] = inserto;
                    *bandera = 0;
                    return;
                } else {
                    printf("ERROR: la carpeta \"%s\" No Exite.\n", carpes[carpe_actual].bname);
                    *bandera = 0;
                    return;
                }
            }
        }
    }
}

void D_exist_n1(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera) {
    bloque_apuntadores apunta2;
    char apuntad = 'a';
    for (int i = 0; i < 16 && *bandera; i++) {
        if (apunta->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apunta2, sizeof (bloque_apuntadores), 1, disco);
            D_exist_n0(disco, sb, &apunta2, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, bandera);
        } else {
            for (int j = 0; j < 16; j++)
                apunta2.b_pointers[j] = -1;
            apunta->b_pointers[i] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apuntad, sizeof (char), 1, disco);
            primer_b_f(disco, sb);
            D_exist_n0(disco, sb, &apunta2, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, bandera);
            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fwrite(&apunta2, sizeof (bloque_apuntadores), 1, disco);
        }
    }
}

void D_exist_n2(FILE *disco, SB *sb, bloque_apuntadores *apunta, Sesion *sesion, carpetas *carpes, int carpe_actual, int ino_padre, int ino_actual, char *nombre, int p, char*path, int *bandera) {
    bloque_apuntadores apunta2;
    char apuntad = 'a';
    for (int i = 0; i < 16 && *bandera; i++) {
        if (apunta->b_pointers[i] != -1) {
            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apunta2, sizeof (bloque_apuntadores), 1, disco);
            D_exist_n1(disco, sb, &apunta2, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, bandera);
        } else {
            for (int j = 0; j < 16; j++)
                apunta2.b_pointers[j] = -1;
            apunta->b_pointers[i] = sb->s_first_blo;
            fseek(disco, sb->s_bm_block_start + sb->s_first_blo * sizeof (char), SEEK_SET);
            fwrite(&apuntad, sizeof (char), 1, disco);
            primer_b_f(disco, sb);
            D_exist_n1(disco, sb, &apunta2, sesion, carpes, carpe_actual, ino_padre, ino_actual, nombre, p, path, bandera);
            fseek(disco, sb->s_block_start + apunta->b_pointers[i] * sizeof (bloque_apuntadores), SEEK_SET);
            fwrite(&apunta2, sizeof (bloque_apuntadores), 1, disco);
        }
    }
}

int encontrar_id(Montaje *montajes, int j, char *id) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(montajes[j].lista[i].id, id) == 0) {
            return i;
        }
    }
    return -1;
}
