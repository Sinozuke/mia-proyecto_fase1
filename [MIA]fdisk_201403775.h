#ifndef FDISK_H
#define FDISK_H

void crear_pp(char *nombre, char *path, int tamano, char fit, int unit);
void crear_pl(char *nombre, char *path, int tamano, char fit, int unit);
void crear_pe(char *nombre, char *path, int tamano, char fit, int unit);
void eliminar_particion(char *name, char *path, char tipo);
void modificar_tamano_particion(char *name, char*path, char unit, int add);

#endif /* FDISK_H */

