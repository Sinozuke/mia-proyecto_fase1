#include "[MIA]Datos_201403775.h"
#ifndef REPORTES_H
#define REPORTES_H

void rep_mbr(Montaje *montaje, char *id, char *path);
void rep_disk(Montaje *montaje, char *id, char *path);
void rep_bm_inode(Montaje *montaje, char *id, char *path);
void rep_bm_block(Montaje *montaje, char *id, char *path);
void rep_sb(Montaje *montaje, char *id, char *path);
void rep_block(Montaje *montaje, char *id, char *path);
void rep_inode(Montaje *montaje, char *id, char *path);
void rep_tree(Montaje *montaje, char *id, char *path);
void rep_journaling(Montaje *montaje, char *id, char *path);


#endif /* REPORTES_H */

