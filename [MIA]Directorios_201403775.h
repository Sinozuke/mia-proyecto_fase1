#include "[MIA]Datos_201403775.h"
#ifndef DIRECTORIOS_H
#define DIRECTORIOS_H

void crear_Directorio(Montaje *montajes,Sesion *sesion, int p, char *id, char *path, int permisos);
void renombrar_Directorio();
void Eliminar_Directorio();
int encontrar_id(Montaje *montajes, int j, char *id);

#endif /* DIRECTORIOS_H */

