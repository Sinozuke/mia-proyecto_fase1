#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]users_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);
void obtener_cont_archivo(char *contenido, inodo *ino, FILE *disco, SB *sb);
int contar_saltos(char *cont);
void almacenar_registros(Registro *regis, char *cont);
void vaciar(Registro *registro, int numero);
void encontrar_id_anterior(Registro *registros, int *retorno, int cantidad, char *nombre, char tipo);
void eliminar_id(Registro *registros, int *retorno, int cantidad, char *nombre, char tipo);
void rtc(Registro *registro, int cantidad, char *dest, int longitud);
int contar_g_u(Registro *registros, int cantidad, char tipo);
int lon_contenido(Registro *registro, int cantidad);
void editar_archivo(FILE *disco, inodo *ino, SB *sb, char *cont);
void primer_b_f(FILE *disco, SB *sb);
void editar_archivo_nivel0(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);
void editar_archivo_nivel1(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);
void editar_archivo_nivel2(FILE *disco, bloque_apuntadores *apunt, SB *sb, int *recorrido, int longitud, char *contenido);

void crear_usuario(Montaje *montaje, Sesion *sesion, char *nombre, char *grupo, char *password, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(nombre) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(grupo) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(password) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != '2' && encontrado.status != '3') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB sb;
    inodo ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fread(&ino, sizeof (inodo), 1, disco);

    char contenido[ino.i_size];
    strcpy(contenido, "");
    obtener_cont_archivo(contenido, &ino, disco, &sb);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros + 1];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    int bandera = 1;
    int bandera2 = 0;
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].grupo, nombre) == 0) {
                if (registros[i].id != 0) {
                    printf("ERROR: El Usuario ya Exite.\n");
                    ino.i_atime = time(NULL);
                    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
                    fwrite(&ino, sizeof (inodo), 1, disco);
                    fclose(disco);
                    return;
                } else {
                    printf("- Reactivando Usuario \"%s\" -\n", nombre);
                    encontrar_id_anterior(registros, &registros[i].id, numero_registros, nombre, 'u');
                    printf("- Reactivacion Completa -\n");
                    bandera = 0;
                }
            }
        }
    }
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'g') {
            if (strcmp(registros[i].grupo, grupo) == 0) {
                if (registros[i].id == 0) {
                    bandera2 = 1;
                }
            }
        }
    }

    if (bandera2) {
        printf("ERROR: el grupo especificado se encuentra actualmente eliminado.\n");
        ino.i_atime = time(NULL);
        fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
        fwrite(&ino, sizeof (inodo), 1, disco);
        fclose(disco);
        return;
    }



    if (bandera) {
        registros[numero_registros].id = contar_g_u(registros, numero_registros, 'u') + 1;
        strcpy(registros[numero_registros].grupo, grupo);
        registros[numero_registros].tipo = 'u';
        strcpy(registros[numero_registros].username, nombre);
        strcpy(registros[numero_registros].password, password);
    }
    int conte = lon_contenido(registros, numero_registros);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros, nuevo_contenido, conte);

    ino.i_size = conte;
    editar_archivo(disco, &ino, &sb, nuevo_contenido);

    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&sb, sizeof (SB), 1, disco);

    fclose(disco);
    printf("Creacion Completa.\n");
}

void eliminar_usuario(Montaje *montaje, Sesion *sesion, char *nombre, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != '2' && encontrado.status != '3') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB sb;
    inodo ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fread(&ino, sizeof (inodo), 1, disco);

    char contenido[ino.i_size];
    strcpy(contenido, "");
    obtener_cont_archivo(contenido, &ino, disco, &sb);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    
    int bandera = 1;
    
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, nombre) == 0) {
                if (registros[i].id != 0) {
                    eliminar_id(registros, &registros[i].id, numero_registros, nombre,'u');
                    bandera = 0;
                } else {
                    printf("ERROR: El Usuario ya se ha eliminado.\n");
                    ino.i_atime = time(NULL);
                    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
                    fwrite(&ino, sizeof (inodo), 1, disco);
                    fclose(disco);
                    return;
                }
            }
        }
    }
    
    if(bandera){
        printf("ERROR: no se ha encontrado ningun usuario como  \"%s\"\n",nombre);
        ino.i_atime = time(NULL);
        fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
        fwrite(&ino, sizeof (inodo), 1, disco);
        fclose(disco);
        return;
    }

    int conte = lon_contenido(registros, numero_registros-1);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros-1, nuevo_contenido, conte);

    ino.i_size=conte;
    editar_archivo(disco, &ino, &sb, nuevo_contenido);

    fseek(disco, sb.s_inode_start + sizeof (inodo), SEEK_SET);
    fwrite(&ino, sizeof (inodo), 1, disco);
    fseek(disco, encontrado.comienzo, SEEK_SET);
    fwrite(&sb, sizeof (SB), 1, disco);

    fclose(disco);
    printf("Eliminacion Completa.\n");

}