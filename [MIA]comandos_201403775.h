#ifndef COMANDOS_H_INCLUDED
#define COMANDOS_H_INCLUDED
#include "[MIA]Datos_201403775.h"
void mkdisk(char *token);
void rmdisk(Montaje *montajes, char *token);
void fdisk(char *token);
void mount(Montaje *mont, char *token);
void umount(Montaje *mont, char *token);
void rep(Montaje *montaje, char *token);
void exec(char *token,Montaje *montaje,Sesion *sesion);
int opcion(char *token);


void mkfs(Montaje *montaje, char* token);
void mkfile();

void clogin(Montaje *montaje, Sesion *sesion, char *token);

void mkgrp(Montaje *montaje, Sesion *sesion, char *token);
void rmgrp(Montaje *montaje, Sesion *sesion, char *token);

void mkusr(Montaje *montaje, Sesion *sesion, char *token);
void rmusr(Montaje *montaje, Sesion *sesion, char *token);

void vmkdir(Montaje *montaje, Sesion *sesion, char *token);



void disk_free();
void disk_Used();



void vchmod();

void cat();
void rm();
void edit();
void ren();

void cp();
void mv();
void find();
void vchown();
void chgrp();
void vunlink();
void tune2fs();
void recovery();
void loss();


#endif // COMANDOS_H_INCLUDED
