#ifndef AUX_COMANDOS_H
#define AUX_COMANDOS_H

int parametros(char *token);
int longitud_real(char *a);
char *valor_real(int a, char *b);
char *ncomillas(char *a);
int k_m(char *valor);
int pasa(char *token, int opcino1, int opcion2);
char BF_FF_WF(char *valor);
char Fast_Full(char *valor);
int isnumero(char *token);
int name_rep(char *token);
int pasa2(char *token);
int ext2_ext3(char *token);

#endif /* AUX_COMANDOS_H */

