#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_tree(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_dot[100] = "dot -Tjpg -o ";
    char comando_abertura[100] = "gnome-open ";

    strcat(comando_dot, directorio);
    strcat(comando_dot, ".jpg TREE.dot");

    strcat(comando_abertura, directorio);
    strcat(comando_abertura, ".jpg");


    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;

    reporte = fopen("TREE.dot", "w");

    SB sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=record];\n");
    fprintf(reporte, "rankdir=LR;\n");

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    char bloque = 'v';

    fseek(disco, sb.s_bm_inode_start, SEEK_SET);

    inodo ino;
    bloque_carpeta carpeta;
    bloque_archivo archivo;
    bloque_apuntadores apuntador;

    for (int i = 0; i < sb.s_inodes_count; i++) {
        fread(&bloque, sizeof (char), 1, disco);
        if (bloque == '1') {
            fseek(disco, sb.s_inode_start + i * sizeof (inodo), SEEK_SET);
            fread(&ino, sizeof (inodo), 1, disco);
            fprintf(reporte, "\t\"i%i\" [label=\"", i);
            fprintf(reporte, "{");
            fprintf(reporte, "<here>Inodo %i", i);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_nlink|");
            fprintf(reporte, "%i", ino.i_nlink);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_pathlink|");
            fprintf(reporte, "%s", ino.i_pathlink);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_uid|");
            fprintf(reporte, "%i", ino.i_uid);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_gid|");
            fprintf(reporte, "%i", ino.I_gid);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_size|");
            fprintf(reporte, "%i", ino.i_size);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_atime|");
            fprintf(reporte, "%s", ctime(&ino.i_atime));
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_ctime|");
            fprintf(reporte, "%s", ctime(&ino.i_ctime));
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_mtime|");
            fprintf(reporte, "%s", ctime(&ino.i_mtime));
            fprintf(reporte, "}|");

            for (int j = 0; j < 15; j++) {
                fprintf(reporte, "{");
                fprintf(reporte, "i_block_%i|", j + 1);
                fprintf(reporte, "%i|<f%i>", ino.i_block[j], j);
                fprintf(reporte, "}|");
            }

            fprintf(reporte, "{");
            fprintf(reporte, "i_type|");
            fprintf(reporte, "%c", ino.i_type);
            fprintf(reporte, "}|");

            fprintf(reporte, "{");
            fprintf(reporte, "i_perm|");
            fprintf(reporte, "%i", ino.i_perm);
            fprintf(reporte, "}");

            fprintf(reporte, "\"];");

            for (int j = 0; j < 15; j++) {
                if (ino.i_block[j] != -1)
                    fprintf(reporte, "\"i%i\":<f%i>->\"b%i\":<here>;\n", i, j, ino.i_block[j]);
            }
        }
        fseek(disco, sb.s_bm_inode_start + i * sizeof (char) + sizeof (char), SEEK_SET);
    }

    for (int i = 0; i < sb.s_blocks_count; i++) {
        fseek(disco, sb.s_bm_block_start + i * sizeof (char), SEEK_SET);
        fread(&bloque, sizeof (char), 1, disco);
        if (bloque == 'd') {
            fprintf(reporte, "\"b%i\" [label=\"", i);
            fprintf(reporte, "{");
            fprintf(reporte, "<here>Bloque Carpeta %i", i);
            fprintf(reporte, "}|");
            fprintf(reporte, "{");
            fprintf(reporte, "b_name|");
            fprintf(reporte, "b_inodo");
            fprintf(reporte, "}|");

            fseek(disco, sb.s_block_start + i * sizeof (bloque_carpeta), SEEK_SET);
            fread(&carpeta, sizeof (bloque_carpeta), 1, disco);

            for (int j = 0; j < 4; j++) {
                fprintf(reporte, "{");
                if (carpeta.b_content[j].b_inodo == -1) {
                    fprintf(reporte, "-|");
                } else {
                    fprintf(reporte, "%s|", carpeta.b_content[j].bname);
                }
                fprintf(reporte, "%i|<f%i>", carpeta.b_content[j].b_inodo, j);
                fprintf(reporte, "}|");
            }

            fprintf(reporte, "\"];\n");

            for (int j = 0; j < 4; j++) {
                if (carpeta.b_content[j].b_inodo != -1) 
                    if(strcmp(carpeta.b_content[j].bname,".")!=0 && strcmp(carpeta.b_content[j].bname,"..")!=0)
                        fprintf(reporte, "\"b%i\":<f%i>->\"i%i\":<here>;\n", i, j, carpeta.b_content[j].b_inodo);
            }

        } else if (bloque == 'f') {
            fseek(disco, sb.s_block_start + i * sizeof (bloque_archivo), SEEK_SET);
            fread(&archivo, sizeof (bloque_archivo), 1, disco);
            fprintf(reporte, "\"b%i\" [label=\"", i);
            fprintf(reporte, "{");
            fprintf(reporte, "<here>Bloque Archivo %i", i);
            fprintf(reporte, "}|");
            fprintf(reporte, "{");
            for (int j = 0; j < 64; j++) {
                if (archivo.b_content[j] == '\n')
                    fprintf(reporte, "\\n");
                else if (archivo.b_content[j] != '\0')
                    fprintf(reporte, "%c", archivo.b_content[j]);
            }
            fprintf(reporte, "}|");
            fprintf(reporte, "\"];\n");
        } else if (bloque == 'a') {
            fseek(disco, sb.s_block_start + i * sizeof (bloque_apuntadores), SEEK_SET);
            fread(&apuntador, sizeof (bloque_apuntadores), 1, disco);
            fprintf(reporte, "b%i\" [label=\"", i);
            fprintf(reporte, "{");
            fprintf(reporte, "<here>Bloque Apuntadores %i", i);
            fprintf(reporte, "}|");
            for (int j = 0; j < 16; j++) {
                fprintf(reporte, "{");
                fprintf(reporte, "%i|<f%i>", apuntador.b_pointers[j], j);
                fprintf(reporte, "}|");
            }
            fprintf(reporte, "\"];\n");
            for (int j = 0; j < 16; j++) {
                if (apuntador.b_pointers[j] != -1)
                    fprintf(reporte, "\"b%i\":<f%i>->\"b%i\":<here>;\n", i, j, apuntador.b_pointers[j]);
            }
        }
    }

    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}