#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "[MIA]reportes_201403775.h"

int encontrar_id(Montaje *montajes, int j, char *id);

int crear_directorios(char *path);

void rep_bm_inode(Montaje *montaje, char *id, char *path) {

    char directorio[strlen(path)];
    strcpy(directorio, path);

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    char comando_abertura[100] = "gnome-open ";


    strcat(comando_abertura, directorio);

    FILE *disco;

    disco = fopen(path_disco, "r+b");

    FILE *reporte;

    reporte = fopen(directorio, "w");

    int contador = 0;

    SB sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB), 1, disco);
    char inode = 'v';

    fseek(disco, sb.s_bm_inode_start, SEEK_SET);

    for (int i = 0; i < sb.s_inodes_count; i++) {
        fread(&inode, sizeof (char), 1, disco);
        fprintf(reporte, " %c ", inode);
        contador++;
        if (contador == 20) {
            contador = 0;
            fprintf(reporte, "\n");
        }
    }

    fclose(reporte);
    fclose(disco);

    system(comando_abertura);
    printf("-- REPORTE GENERADO --\n");
}