#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "[MIA]rmdisk_201403775.h"

void eliminar_disco(Montaje *montajes, char *path) {
    if (access(path, F_OK) == -1) {
        printf("ERROR: no exite el directorio o Disco con el nombre \"%s\"\n", path);
        printf("********ELIMINACION DE DISCO FALLIDA********\n\n");
        return;
    }

    for (int i = 0; i < 26; i++) {
        if (strcmp(montajes[i].path, path) == 0) {
            for (int j = 0; j < 100; j++) {
                montajes[i].lista[j].comienzo = -1;
                strcpy(montajes[i].lista[j].id, "");
                montajes[i].lista[j].letra = '\0';
                strcpy(montajes[i].lista[j].nombre, "");
                montajes[i].lista[j].numero = -1;
                montajes[i].lista[j].size = -1;
                montajes[i].lista[j].status = '\0';
                montajes[i].lista[j].tipo = '\0';
            }
            strcpy(montajes[i].path, "");
            montajes[i].indice = '\0';
        }
    }

    if (unlink(path) == 0) {
        printf("********ELIMINACION DE DISCO EXITOSA********\n\n");
    } else {
        printf("********ELIMINACION DE DISCO FALLIDA********\n\n");
    }
}

