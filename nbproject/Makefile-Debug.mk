#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/\[MIA\]Aux_Busqueda_ino_201403775.o \
	${OBJECTDIR}/\[MIA\]Aux_comandos_201403775.o \
	${OBJECTDIR}/\[MIA\]Login_201403775.o \
	${OBJECTDIR}/\[MIA\]Mount_201403775.o \
	${OBJECTDIR}/\[MIA\]cat_201403775.o \
	${OBJECTDIR}/\[MIA\]comandos2_201403775.o \
	${OBJECTDIR}/\[MIA\]comandos_201403775.o \
	${OBJECTDIR}/\[MIA\]fdisk_201403775.o \
	${OBJECTDIR}/\[MIA\]groups_201403775.o \
	${OBJECTDIR}/\[MIA\]main_201403775.o \
	${OBJECTDIR}/\[MIA\]mkdir_201403775.o \
	${OBJECTDIR}/\[MIA\]mkdisk_201403775.o \
	${OBJECTDIR}/\[MIA\]mkfile_201403775.o \
	${OBJECTDIR}/\[MIA\]mkfs_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_block_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_bm_block_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_bm_inode_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_disk_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_inode_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_journaling_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_mbr_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_sb_201403775.o \
	${OBJECTDIR}/\[MIA\]reporte_tree_201403775.o \
	${OBJECTDIR}/\[MIA\]rmdisk_201403775.o \
	${OBJECTDIR}/\[MIA\]users_201403775.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/_mia_proyecto_fase1

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/_mia_proyecto_fase1: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/_mia_proyecto_fase1 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/\[MIA\]Aux_Busqueda_ino_201403775.o: \[MIA\]Aux_Busqueda_ino_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]Aux_Busqueda_ino_201403775.o \[MIA\]Aux_Busqueda_ino_201403775.c

${OBJECTDIR}/\[MIA\]Aux_comandos_201403775.o: \[MIA\]Aux_comandos_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]Aux_comandos_201403775.o \[MIA\]Aux_comandos_201403775.c

${OBJECTDIR}/\[MIA\]Login_201403775.o: \[MIA\]Login_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]Login_201403775.o \[MIA\]Login_201403775.c

${OBJECTDIR}/\[MIA\]Mount_201403775.o: \[MIA\]Mount_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]Mount_201403775.o \[MIA\]Mount_201403775.c

${OBJECTDIR}/\[MIA\]cat_201403775.o: \[MIA\]cat_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]cat_201403775.o \[MIA\]cat_201403775.c

${OBJECTDIR}/\[MIA\]comandos2_201403775.o: \[MIA\]comandos2_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]comandos2_201403775.o \[MIA\]comandos2_201403775.c

${OBJECTDIR}/\[MIA\]comandos_201403775.o: \[MIA\]comandos_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]comandos_201403775.o \[MIA\]comandos_201403775.c

${OBJECTDIR}/\[MIA\]fdisk_201403775.o: \[MIA\]fdisk_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]fdisk_201403775.o \[MIA\]fdisk_201403775.c

${OBJECTDIR}/\[MIA\]groups_201403775.o: \[MIA\]groups_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]groups_201403775.o \[MIA\]groups_201403775.c

${OBJECTDIR}/\[MIA\]main_201403775.o: \[MIA\]main_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]main_201403775.o \[MIA\]main_201403775.c

${OBJECTDIR}/\[MIA\]mkdir_201403775.o: \[MIA\]mkdir_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]mkdir_201403775.o \[MIA\]mkdir_201403775.c

${OBJECTDIR}/\[MIA\]mkdisk_201403775.o: \[MIA\]mkdisk_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]mkdisk_201403775.o \[MIA\]mkdisk_201403775.c

${OBJECTDIR}/\[MIA\]mkfile_201403775.o: \[MIA\]mkfile_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]mkfile_201403775.o \[MIA\]mkfile_201403775.c

${OBJECTDIR}/\[MIA\]mkfs_201403775.o: \[MIA\]mkfs_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]mkfs_201403775.o \[MIA\]mkfs_201403775.c

${OBJECTDIR}/\[MIA\]reporte_block_201403775.o: \[MIA\]reporte_block_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_block_201403775.o \[MIA\]reporte_block_201403775.c

${OBJECTDIR}/\[MIA\]reporte_bm_block_201403775.o: \[MIA\]reporte_bm_block_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_bm_block_201403775.o \[MIA\]reporte_bm_block_201403775.c

${OBJECTDIR}/\[MIA\]reporte_bm_inode_201403775.o: \[MIA\]reporte_bm_inode_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_bm_inode_201403775.o \[MIA\]reporte_bm_inode_201403775.c

${OBJECTDIR}/\[MIA\]reporte_disk_201403775.o: \[MIA\]reporte_disk_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_disk_201403775.o \[MIA\]reporte_disk_201403775.c

${OBJECTDIR}/\[MIA\]reporte_inode_201403775.o: \[MIA\]reporte_inode_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_inode_201403775.o \[MIA\]reporte_inode_201403775.c

${OBJECTDIR}/\[MIA\]reporte_journaling_201403775.o: \[MIA\]reporte_journaling_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_journaling_201403775.o \[MIA\]reporte_journaling_201403775.c

${OBJECTDIR}/\[MIA\]reporte_mbr_201403775.o: \[MIA\]reporte_mbr_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_mbr_201403775.o \[MIA\]reporte_mbr_201403775.c

${OBJECTDIR}/\[MIA\]reporte_sb_201403775.o: \[MIA\]reporte_sb_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_sb_201403775.o \[MIA\]reporte_sb_201403775.c

${OBJECTDIR}/\[MIA\]reporte_tree_201403775.o: \[MIA\]reporte_tree_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]reporte_tree_201403775.o \[MIA\]reporte_tree_201403775.c

${OBJECTDIR}/\[MIA\]rmdisk_201403775.o: \[MIA\]rmdisk_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]rmdisk_201403775.o \[MIA\]rmdisk_201403775.c

${OBJECTDIR}/\[MIA\]users_201403775.o: \[MIA\]users_201403775.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/\[MIA\]users_201403775.o \[MIA\]users_201403775.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/_mia_proyecto_fase1

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
