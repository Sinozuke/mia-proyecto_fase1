#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "[MIA]comandos_201403775.h"
#define MAXLON 300

Montaje montaje[26];
Sesion sesion = {-1, -1, -1, -1};
char* token;
char cadena[MAXLON + 1];

int main() { 
    while (1) {
        fgets(cadena, MAXLON, stdin);
        for (int i = 0; cadena[i] != '\0'; i++)
            if (cadena[i] == '\n')
                cadena[i] = '\0';
        token = strtok(cadena, " ");
        while (token) {
            switch (opcion(token)) {
                case 1:
                    mkdisk(token);
                    break;
                case 2:
                    rmdisk(montaje, token);
                    break;
                case 3:
                    fdisk(token);
                    break;
                case 4:
                    mount(montaje, token);
                    break;
                case 5:
                    umount(montaje, token);
                    break;
                case 6:
                    rep(montaje, token);
                    break;
                case 7:
                    exec(token,montaje,&sesion);
                    break;
                case 9:
                    printf("COMANDO DF INVOCADO\n");
                    break;
                case 10:
                    printf("COMANDO DU INVOCADO\n");
                    break;
                case 11:
                    mkfs(montaje, token);
                    break;
                case 12:
                    if (sesion.UID != -1) {
                        printf("ERROR: Deve cerrar sesion primero antes de iniciar otra sesion.\n");
                    } else {
                        clogin(montaje, &sesion, token);
                    }
                    break;
                case 13:
                    if (sesion.UID == -1) {
                        printf("ERROR: no hay ninguna Sesion Iniciada.\n");
                    } else {
                        sesion.GID = -1;
                        sesion.UID = -1;
                        sesion.lista = -1;
                        sesion.montaje = -1;
                        printf("Sesion Cerrada.\n");
                    }
                    break;
                case 14:
                    if (sesion.UID != 1) {
                        printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                    } else {
                        mkgrp(montaje, &sesion, token);
                    }
                    break;
                case 15:
                    if (sesion.UID != 1) {
                        printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                    } else {
                        rmgrp(montaje, &sesion, token);
                    }
                    break;
                case 16:
                    if (sesion.GID != 1) {
                        printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                    } else {
                        mkusr(montaje, &sesion, token);
                    }
                    break;
                case 17:
                    if (sesion.UID != 1) {
                        printf("ERROR: no estas logeado como root para realizar esta operacion.\n");
                    } else {
                        rmusr(montaje, &sesion, token);
                    }
                    break;
                case 18:
                    printf("COMANDO CHMOD INVOCADO\n");
                    break;
                case 19:
                    printf("COMANDO MKFILE INVOCADO\n");
                    break;
                case 20:
                    printf("COMANDO CAT INVOCADO\n");
                    break;
                case 21:
                    printf("COMANDO RM INVOCADO\n");
                    break;
                case 22:
                    printf("COMANDO EDIT INVOCADO\n");
                    break;
                case 23:
                    printf("COMANDO REN INVOCADO\n");
                    break;
                case 24:
                    if (sesion.UID == -1)
                        printf("ERROR: no hay ninguna Sesion Iniciada.\n");
                    else
                        vmkdir(montaje,&sesion,token);
                    break;
                case 25:
                    printf("COMANDO CP INVOCADO\n");
                    break;
                case 26:
                    printf("COMANDO MV INVOCADO\n");
                    break;
                case 27:
                    printf("COMANDO FIND INVOCADO\n");
                    break;
                case 28:
                    printf("COMANDO CHOWN INVOCADO\n");
                    break;
                case 29:
                    printf("COMANDO CHGRP INVOCADO\n");
                    break;
                case 30:
                    printf("COMANDO LN INVOCADO\n");
                    break;
                case 31:
                    printf("COMANDO UNLINK INVOCADO\n");
                    break;
                case 32:
                    printf("COMANDO TUNE2FS INVOCADO\n");
                    break;
                case 33:
                    printf("COMANDO RECOVERY INVOCADO\n");
                    break;
                case 34:
                    printf("COMANDO LOSS INVOCADO\n");
                    break;
                default:
                    printf("ERROR: el comando \"%s\" no reconocido\n", token);
                    break;
                case 8:
                    printf("----------Saliendo del sistema----------\n");
                    return 0;
            }
            while (token)
                token = strtok(NULL, " ");
        }
    }
}
/*
void dec_bin(int src,char *dest){
    char buffer[20];
    char num[3];
    for(int i=0;i<9;i++)
        dest[i]='\0';
    sprintf(buffer, "%d", src);
    if(strlen(buffer)==1){
        num[0]='0';
        num[1]='0';
        num[2]=buffer[0];
    }else if(strlen(buffer)==2){
        num[0]='0';
        num[1]=buffer[0];
        num[2]=buffer[1];
    }else{
        strcpy(num,buffer);
    }
    
    int contador1=2;
    int contador2=0;
    
    for(int i=0;i<9;i++){
        if(contador1<0){
            contador1=2;
            contador2++;
        }
        dest[i]=((num[contador2] & ( 1 << contador1 )) >> contador1)+'0';
        contador1--;
    }
}
 */